# Jeeves

[![pipeline status](https://gitlab.com/Veroxis/jeeves/badges/master/pipeline.svg)](https://gitlab.com/Veroxis/jeeves/-/commits/master)

[Cargo Doc](https://veroxis.gitlab.io/jeeves/doc/jeeves/index.html)

Jeeves is a command line utility which acts as a layer above your OS package manager. It provides an `interactive package browser` within the CLI.

It's distributed `statically compiled` for `x86_64` using [musl libc](<https://musl.libc.org/>).

## Features

- initially built to be usable on the bare [archlinux install medium](<https://archlinux.org/download/>)
- optional snapshot management before updating by using [timeshift](<https://github.com/teejee2008/timeshift>)
- package browser to _add_ and _remove_ packages using [skim](<https://crates.io/crates/skim>)
  - search for packages using `jeeves add` and `jeeves rm`, it will auto detect installed package managers:
    - Arch based: `pacman` and `yay`
    - Debian based: `dpkg` and `apt-get`
    - Alpine based: `apk`
    - RHEL based: `dnf`
  - flatpak support using `jeeves add --flatpak`
- genereal system updater for updating `system packages`, `flatpak`, `brew` and other developer centric package managers which i'm using like `rustup`
- an self updater and version selector using Gitlab CI for building and the Gitlab API for releases
- online code documentation on [Gitlab Pages](<https://veroxis.gitlab.io/arch_helper/doc/arch_helper/index.html>)

## Installation

```bash
# Install
curl -o /tmp/jeeves "https://veroxis.gitlab.io/jeeves/release/jeeves"
chmod +x /tmp/jeeves
/tmp/jeeves update-self

# Test it worked
jeeves --version
```

## Examples

```sh
jeeves add
```

![Package Install](https://veroxis.gitlab.io/jeeves/assets/images/jeeves_install.png)

```sh
jeeves rm
```

![Package Uninstall](https://veroxis.gitlab.io/jeeves/assets/images/jeeves_uninstall.png)

## License

Licensed under either of [Apache License, Version 2.0](<LICENSE-APACHE>) or [MIT license](<LICENSE-MIT>) at your option.
