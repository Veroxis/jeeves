## [1.5.90](https://gitlab.com/Veroxis/jeeves/compare/1.5.89...1.5.90) (10/15/2024)


### Bug Fixes

* **deps:** lock file maintenance ([a01b5fd](https://gitlab.com/Veroxis/jeeves/commit/a01b5fd48c574ebec4a292245e8997c45085281d))
* **deps:** lock file maintenance ([f49ef1d](https://gitlab.com/Veroxis/jeeves/commit/f49ef1dd98897c9368a103c57eca450d5df81c91))
* **deps:** lock file maintenance ([ec2eee0](https://gitlab.com/Veroxis/jeeves/commit/ec2eee0dafe8bf2fe4b752da931a1fb3cd631392))
* **deps:** lock file maintenance ([5fcfaf4](https://gitlab.com/Veroxis/jeeves/commit/5fcfaf4a7a9811854f7202e06ddb0a9248b13faf))
* **deps:** lock file maintenance ([b9e8a04](https://gitlab.com/Veroxis/jeeves/commit/b9e8a04a4dbb20feb6b9f0eb55d30c823b765b30))
* **deps:** lock file maintenance ([6298306](https://gitlab.com/Veroxis/jeeves/commit/6298306e97c1228f7b44bd2167ae922065f001a0))
* **deps:** lock file maintenance ([fb37b1f](https://gitlab.com/Veroxis/jeeves/commit/fb37b1f299d18f2e7b3d9cc47186c27ae272813b))
* **deps:** lock file maintenance ([4466e82](https://gitlab.com/Veroxis/jeeves/commit/4466e826dd5275dfbdc066ec2005a885387207e5))
* **deps:** lock file maintenance ([a5e9cc8](https://gitlab.com/Veroxis/jeeves/commit/a5e9cc8ad56a63146a21c632ee383efc2dc69a7e))
* **deps:** lock file maintenance ([b545d7b](https://gitlab.com/Veroxis/jeeves/commit/b545d7b3478f5d0baf177b293f096982f56c1cee))
* **deps:** lock file maintenance ([d6e5056](https://gitlab.com/Veroxis/jeeves/commit/d6e5056207e82d93e9512d3d2e4866c8a174215b))
* **deps:** lock file maintenance ([11bfab9](https://gitlab.com/Veroxis/jeeves/commit/11bfab9077c5f3816975eab778229f54a6ecd792))
* **deps:** lock file maintenance ([9036197](https://gitlab.com/Veroxis/jeeves/commit/9036197beb4a80769e45a57635409260def814df))
* **deps:** lock file maintenance ([4b21057](https://gitlab.com/Veroxis/jeeves/commit/4b21057a6fe7b1c03bac849ace5811ef40e9bf46))
* **deps:** lock file maintenance ([52b1695](https://gitlab.com/Veroxis/jeeves/commit/52b1695e075183ddc879cafd5562fab191e6df00))
* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.79.0 ([0415661](https://gitlab.com/Veroxis/jeeves/commit/041566175161f680cf66111f17037ab09fc98eb7))
* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.80.1 ([9ae54ef](https://gitlab.com/Veroxis/jeeves/commit/9ae54efce3eb4e8a63568c8ef6c11607b35c507c))
* **deps:** update rust crate async-trait to v0.1.81 ([4466af1](https://gitlab.com/Veroxis/jeeves/commit/4466af190830d6fac3a5501632deb4c476c66b8b))
* **deps:** update rust crate async-trait to v0.1.82 ([9868106](https://gitlab.com/Veroxis/jeeves/commit/9868106a6b503dc8526ec60e67edc5646e80093c))
* **deps:** update rust crate async-trait to v0.1.83 ([6f34e82](https://gitlab.com/Veroxis/jeeves/commit/6f34e82b6dc5ca85c88ad2fd3d3691d31edffcd1))
* **deps:** update rust crate clap to v4.5.10 ([550851d](https://gitlab.com/Veroxis/jeeves/commit/550851db1ec7f26d749dad67a4a72e580d139f6c))
* **deps:** update rust crate clap to v4.5.11 ([6d5ab16](https://gitlab.com/Veroxis/jeeves/commit/6d5ab164e507470fcf21037eb4f30514bd514259))
* **deps:** update rust crate clap to v4.5.12 ([df47015](https://gitlab.com/Veroxis/jeeves/commit/df47015ceb043f01fc90bedab742d438c5288a41))
* **deps:** update rust crate clap to v4.5.13 ([338de4f](https://gitlab.com/Veroxis/jeeves/commit/338de4f2968a4085fb5f5458bbe6eab3a300defe))
* **deps:** update rust crate clap to v4.5.14 ([a3b8069](https://gitlab.com/Veroxis/jeeves/commit/a3b80695d37621a8e844e618c35861b9b29fb30e))
* **deps:** update rust crate clap to v4.5.15 ([3ff6a4b](https://gitlab.com/Veroxis/jeeves/commit/3ff6a4bb6e6c47fc2ae58c9497471e3d2df60931))
* **deps:** update rust crate clap to v4.5.16 ([b628d51](https://gitlab.com/Veroxis/jeeves/commit/b628d51261e4eaaffc3a699fda0b680a1b9d42cc))
* **deps:** update rust crate clap to v4.5.17 ([315a18b](https://gitlab.com/Veroxis/jeeves/commit/315a18b07836a062540e4693765c3198c50052ef))
* **deps:** update rust crate clap to v4.5.18 ([67d9c23](https://gitlab.com/Veroxis/jeeves/commit/67d9c23e3e655b615d3b153fce26d8b03da88bc0))
* **deps:** update rust crate clap to v4.5.7 ([6fc29bd](https://gitlab.com/Veroxis/jeeves/commit/6fc29bd0013744f613f6d082b0bb1e0b5fc55ad8))
* **deps:** update rust crate clap to v4.5.8 ([49aa9a8](https://gitlab.com/Veroxis/jeeves/commit/49aa9a8deac04940aa8f3c49129043a877ab61b6))
* **deps:** update rust crate clap to v4.5.9 ([5b6305a](https://gitlab.com/Veroxis/jeeves/commit/5b6305a0039df7f794a39eb79b8285bb754cae15))
* **deps:** update rust crate flate2 to v1.0.31 ([8940bf2](https://gitlab.com/Veroxis/jeeves/commit/8940bf2d90728c14abdbebc844b11ccfe412b216))
* **deps:** update rust crate flate2 to v1.0.32 ([29cdeba](https://gitlab.com/Veroxis/jeeves/commit/29cdeba4dc4c418ff265fb288fa83a30c869bc56))
* **deps:** update rust crate flate2 to v1.0.33 ([de3a8b5](https://gitlab.com/Veroxis/jeeves/commit/de3a8b5dff5bd68d336363d16791f35c93a38745))
* **deps:** update rust crate minreq to v2.12.0 ([64b32df](https://gitlab.com/Veroxis/jeeves/commit/64b32dfe1456ade041a68bf0db965b35166d46bd))
* **deps:** update rust crate serde to v1.0.203 ([1650c99](https://gitlab.com/Veroxis/jeeves/commit/1650c994030c8155823d18d0d32eff1afdc40898))
* **deps:** update rust crate serde to v1.0.204 ([1fd4d23](https://gitlab.com/Veroxis/jeeves/commit/1fd4d239657bd2e504e1faad78fcadcfaccbe1e0))
* **deps:** update rust crate serde to v1.0.205 ([73e909b](https://gitlab.com/Veroxis/jeeves/commit/73e909bcf3f7421835c656cbb9a068789264c75c))
* **deps:** update rust crate serde to v1.0.206 ([521d613](https://gitlab.com/Veroxis/jeeves/commit/521d613eca95afaacf39ea91e99ff31c401d5bb8))
* **deps:** update rust crate serde to v1.0.207 ([adfa10f](https://gitlab.com/Veroxis/jeeves/commit/adfa10f4964bab5950a961b6342ad516159048e2))
* **deps:** update rust crate serde to v1.0.208 ([4249293](https://gitlab.com/Veroxis/jeeves/commit/42492934efbc08313cab60412795dfb50925bd64))
* **deps:** update rust crate serde to v1.0.209 ([d023726](https://gitlab.com/Veroxis/jeeves/commit/d02372691729e64035f7003580d4871120a4f0d4))
* **deps:** update rust crate serde to v1.0.210 ([dc5a363](https://gitlab.com/Veroxis/jeeves/commit/dc5a363d1455ecc9141727abddbae6100f7361c3))
* **deps:** update rust crate serde_json to v1.0.118 ([49f456a](https://gitlab.com/Veroxis/jeeves/commit/49f456a566c3729b882eb3af3fd6d199cb450400))
* **deps:** update rust crate serde_json to v1.0.119 ([7a32b8c](https://gitlab.com/Veroxis/jeeves/commit/7a32b8cabd9182190aabcbfbc4041a9ce9767545))
* **deps:** update rust crate serde_json to v1.0.120 ([3e94869](https://gitlab.com/Veroxis/jeeves/commit/3e94869f228fe1d962b25d3d337e88c9aa8aa7b6))
* **deps:** update rust crate serde_json to v1.0.121 ([676ba7f](https://gitlab.com/Veroxis/jeeves/commit/676ba7f256c7369f29a3dc793ba046447070dbda))
* **deps:** update rust crate serde_json to v1.0.122 ([901f281](https://gitlab.com/Veroxis/jeeves/commit/901f2819c72fbe37de0dd05ad088d8159b035d75))
* **deps:** update rust crate serde_json to v1.0.123 ([7a7b12b](https://gitlab.com/Veroxis/jeeves/commit/7a7b12b5c7de36bbbfc719c0646ef8497f1101ac))
* **deps:** update rust crate serde_json to v1.0.124 ([b86dbf0](https://gitlab.com/Veroxis/jeeves/commit/b86dbf004515ea661a9b9380d747bacf2b66a4f4))
* **deps:** update rust crate serde_json to v1.0.125 ([45d1e18](https://gitlab.com/Veroxis/jeeves/commit/45d1e18c59fb34fb1ee50ab1f3e77ea5f632ef97))
* **deps:** update rust crate serde_json to v1.0.126 ([b36874c](https://gitlab.com/Veroxis/jeeves/commit/b36874c6871e22d9227797941eee2deae87419a2))
* **deps:** update rust crate serde_json to v1.0.127 ([6116fd8](https://gitlab.com/Veroxis/jeeves/commit/6116fd8ca15fce26449b433ae22f3159b7fa5abe))
* **deps:** update rust crate serde_json to v1.0.128 ([ba84f09](https://gitlab.com/Veroxis/jeeves/commit/ba84f09cfafcc929bc5fb3f0b83e6a7e4f1430f2))
* **deps:** update rust crate tar to v0.4.41 ([4167b26](https://gitlab.com/Veroxis/jeeves/commit/4167b26fe07f6f780a1ee39f708683142c05378d))
* **deps:** update rust crate tar to v0.4.42 ([929fe32](https://gitlab.com/Veroxis/jeeves/commit/929fe323510cc88ca5f4a01a0befe9924eca9d42))
* **deps:** update rust crate thiserror to v1.0.61 ([24dde79](https://gitlab.com/Veroxis/jeeves/commit/24dde798b1cdf0318a04119f5245675c8b11e492))
* **deps:** update rust crate thiserror to v1.0.62 ([20a6cf3](https://gitlab.com/Veroxis/jeeves/commit/20a6cf38bff8477e5cbd799237865f0b8ed0a907))
* **deps:** update rust crate thiserror to v1.0.63 ([d3f04fe](https://gitlab.com/Veroxis/jeeves/commit/d3f04feb6bf22c9716c97f666c883e48f94ab52f))
* **deps:** update rust crate thiserror to v1.0.64 ([5f5b1df](https://gitlab.com/Veroxis/jeeves/commit/5f5b1df233585583ec32f87ba7f6a0465305e028))
* **deps:** update rust crate tokio to v1.38.0 ([baa1fcb](https://gitlab.com/Veroxis/jeeves/commit/baa1fcb8369718f933528aee5958880dfb80fdff))
* **deps:** update rust crate tokio to v1.38.1 ([462511a](https://gitlab.com/Veroxis/jeeves/commit/462511a92a8b86f03cc730b41d9bc00ebabf99b1))
* **deps:** update rust crate tokio to v1.39.0 ([f77fb59](https://gitlab.com/Veroxis/jeeves/commit/f77fb59b609310834a49019f13f48d93dacfc516))
* **deps:** update rust crate tokio to v1.39.1 ([c81b82c](https://gitlab.com/Veroxis/jeeves/commit/c81b82c96c121796398dc2eaa5174ae890ef590b))
* **deps:** update rust crate tokio to v1.39.2 ([374042c](https://gitlab.com/Veroxis/jeeves/commit/374042c7b30200208c94c89e3bfec3a6c1eeed00))
* **deps:** update rust crate tokio to v1.39.3 ([3b7f984](https://gitlab.com/Veroxis/jeeves/commit/3b7f98478ecc411247d6495f49920c91f5b8657c))
* **deps:** update rust crate tokio to v1.40.0 ([793c792](https://gitlab.com/Veroxis/jeeves/commit/793c7922b62cdd367ac96503dc7c2d7f94eade0e))

## [1.5.89](https://gitlab.com/Veroxis/jeeves/compare/1.5.88...1.5.89) (5/8/2024)


### Bug Fixes

* **deps:** update rust crate serde_json to v1.0.117 ([bc7aab4](https://gitlab.com/Veroxis/jeeves/commit/bc7aab438c16bf8d13d2d57533b08c80a29c06b4))

## [1.5.88](https://gitlab.com/Veroxis/jeeves/compare/1.5.87...1.5.88) (5/8/2024)


### Bug Fixes

* **deps:** update rust crate serde to v1.0.201 ([3c103ea](https://gitlab.com/Veroxis/jeeves/commit/3c103ea9cb1ac3bbfcc83eda0ae0524cae32ad75))

## [1.5.87](https://gitlab.com/Veroxis/jeeves/compare/1.5.86...1.5.87) (5/7/2024)


### Bug Fixes

* **deps:** update rust crate thiserror to v1.0.60 ([08a4bb7](https://gitlab.com/Veroxis/jeeves/commit/08a4bb7dcbcc0e5a51aabfcf91d4f171ca41ff9b))

## [1.5.86](https://gitlab.com/Veroxis/jeeves/compare/1.5.85...1.5.86) (5/7/2024)


### Bug Fixes

* **deps:** update rust crate semver to v1.0.23 ([84bab40](https://gitlab.com/Veroxis/jeeves/commit/84bab400877ef81cc28a3afe1d16ad64e86bab20))

## [1.5.85](https://gitlab.com/Veroxis/jeeves/compare/1.5.84...1.5.85) (5/1/2024)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.200 ([6dd8400](https://gitlab.com/Veroxis/jeeves/commit/6dd84003bc5776ac29183c960700d58a9bc5046c))

## [1.5.84](https://gitlab.com/Veroxis/jeeves/compare/1.5.83...1.5.84) (5/1/2024)


### Bug Fixes

* **deps:** lock file maintenance ([78a4212](https://gitlab.com/Veroxis/jeeves/commit/78a4212182b2e928f7bf60f6c1ab0bcfd8ec025d))

## [1.5.83](https://gitlab.com/Veroxis/jeeves/compare/1.5.82...1.5.83) (4/29/2024)


### Bug Fixes

* **deps:** update rust crate flate2 to 1.0.30 ([a65e48f](https://gitlab.com/Veroxis/jeeves/commit/a65e48f8fb478d274570a1aa3b054db28facc1c3))

## [1.5.82](https://gitlab.com/Veroxis/jeeves/compare/1.5.81...1.5.82) (4/27/2024)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.199 ([4aa2ed7](https://gitlab.com/Veroxis/jeeves/commit/4aa2ed7d91d2fd489447aa322c92bde110c340da))

## [1.5.81](https://gitlab.com/Veroxis/jeeves/compare/1.5.80...1.5.81) (4/26/2024)


### Bug Fixes

* **deps:** update rust crate minreq to 2.11.2 ([4080a64](https://gitlab.com/Veroxis/jeeves/commit/4080a6486b935e81bce3d6785dc9853809a9eaf8))

## [1.5.80](https://gitlab.com/Veroxis/jeeves/compare/1.5.79...1.5.80) (4/26/2024)


### Bug Fixes

* **deps:** update rust crate flate2 to 1.0.29 ([5c9a24e](https://gitlab.com/Veroxis/jeeves/commit/5c9a24e57bedc0f44c0a3423abaf9de98ef61f33))

## [1.5.79](https://gitlab.com/Veroxis/jeeves/compare/1.5.78...1.5.79) (4/24/2024)


### Bug Fixes

* **deps:** lock file maintenance ([a342ab5](https://gitlab.com/Veroxis/jeeves/commit/a342ab55e33a42d3bf87cc32f46a31e2b84fcce1))

## [1.5.78](https://gitlab.com/Veroxis/jeeves/compare/1.5.77...1.5.78) (4/20/2024)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.59 ([d7bdc63](https://gitlab.com/Veroxis/jeeves/commit/d7bdc635b929bff0ef45f5721d2d649cba162940))

## [1.5.77](https://gitlab.com/Veroxis/jeeves/compare/1.5.76...1.5.77) (4/19/2024)


### Bug Fixes

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.77.2 ([a744774](https://gitlab.com/Veroxis/jeeves/commit/a7447744491c7eca9ad56c04c6a2219a83ced3fa))

## [1.5.76](https://gitlab.com/Veroxis/jeeves/compare/1.5.75...1.5.76) (4/17/2024)


### Bug Fixes

* **deps:** lock file maintenance ([b27d043](https://gitlab.com/Veroxis/jeeves/commit/b27d0430b716b038692a80c99399c27afaf90baa))

## [1.5.75](https://gitlab.com/Veroxis/jeeves/compare/1.5.74...1.5.75) (4/16/2024)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.198 ([a5e11fb](https://gitlab.com/Veroxis/jeeves/commit/a5e11fb1a9eeb0b5364c3d9db2690bd930e02544))

## [1.5.74](https://gitlab.com/Veroxis/jeeves/compare/1.5.73...1.5.74) (4/16/2024)


### Bug Fixes

* **deps:** update rust crate chrono to 0.4.38 ([eb64a42](https://gitlab.com/Veroxis/jeeves/commit/eb64a42d568ae264b4d6dca62a6ae285ebc2f747))

## [1.5.73](https://gitlab.com/Veroxis/jeeves/compare/1.5.72...1.5.73) (4/16/2024)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.116 ([0c3cf7a](https://gitlab.com/Veroxis/jeeves/commit/0c3cf7a34f0570ae62a2c567256b59a2fc9e83d4))

## [1.5.72](https://gitlab.com/Veroxis/jeeves/compare/1.5.71...1.5.72) (4/11/2024)


### Bug Fixes

* **deps:** update rust crate async-trait to 0.1.80 ([a8de6d2](https://gitlab.com/Veroxis/jeeves/commit/a8de6d2b516663409aed5bc82b1a01dfeec07a21))

## [1.5.71](https://gitlab.com/Veroxis/jeeves/compare/1.5.70...1.5.71) (4/3/2024)


### Bug Fixes

* **deps:** lock file maintenance ([7c2d92b](https://gitlab.com/Veroxis/jeeves/commit/7c2d92b6edb71fcd495d21020aba8ca374e4798f))

## [1.5.70](https://gitlab.com/Veroxis/jeeves/compare/1.5.69...1.5.70) (4/2/2024)


### Bug Fixes

* **deps:** update rust crate minreq to 2.11.1 ([6ebc39e](https://gitlab.com/Veroxis/jeeves/commit/6ebc39ee940871732355183a93f481897f9b4808))

## [1.5.69](https://gitlab.com/Veroxis/jeeves/compare/1.5.68...1.5.69) (4/2/2024)


### Bug Fixes

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.77.1 ([b2eed38](https://gitlab.com/Veroxis/jeeves/commit/b2eed3886415476a4b95d5adefbe73ee9dba57c6))

## [1.5.68](https://gitlab.com/Veroxis/jeeves/compare/1.5.67...1.5.68) (3/28/2024)


### Bug Fixes

* **deps:** update rust crate tokio to 1.37.0 ([62a7e43](https://gitlab.com/Veroxis/jeeves/commit/62a7e4325a0901b8e0577a01430d05cf64b24fa2))

## [1.5.67](https://gitlab.com/Veroxis/jeeves/compare/1.5.66...1.5.67) (3/27/2024)


### Bug Fixes

* **deps:** update rust crate chrono to 0.4.37 ([f2b930c](https://gitlab.com/Veroxis/jeeves/commit/f2b930cbd9e564e3ada4ab0aff6ce4b0e4e947ce))

## [1.5.66](https://gitlab.com/Veroxis/jeeves/compare/1.5.65...1.5.66) (3/27/2024)


### Bug Fixes

* **deps:** lock file maintenance ([de87929](https://gitlab.com/Veroxis/jeeves/commit/de8792948bbef19fe7877f92f30d09278e7e6a2f))

## [1.5.65](https://gitlab.com/Veroxis/jeeves/compare/1.5.64...1.5.65) (3/26/2024)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.115 ([1a25765](https://gitlab.com/Veroxis/jeeves/commit/1a25765e4ff94c84161b9e62b4b02da6a3940620))

## [1.5.64](https://gitlab.com/Veroxis/jeeves/compare/1.5.63...1.5.64) (3/25/2024)


### Bug Fixes

* **deps:** update rust crate clap to 4.5.4 ([3ffbad7](https://gitlab.com/Veroxis/jeeves/commit/3ffbad7213e3a19eadb993fd8fa223b02499bd64))

## [1.5.63](https://gitlab.com/Veroxis/jeeves/compare/1.5.62...1.5.63) (3/24/2024)


### Bug Fixes

* **deps:** update rust crate async-trait to 0.1.79 ([14a894f](https://gitlab.com/Veroxis/jeeves/commit/14a894f1a114d6071110f57449f82d97e932aff2))

## [1.5.62](https://gitlab.com/Veroxis/jeeves/compare/1.5.61...1.5.62) (3/24/2024)


### Bug Fixes

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.77.0 ([3afff3b](https://gitlab.com/Veroxis/jeeves/commit/3afff3b116d405024ddf65555e5c663a7452a4df))

## [1.5.61](https://gitlab.com/Veroxis/jeeves/compare/1.5.60...1.5.61) (3/20/2024)


### Bug Fixes

* **deps:** lock file maintenance ([0d195c4](https://gitlab.com/Veroxis/jeeves/commit/0d195c4b90737af8feb884db272389fd7d2f7c6b))

## [1.5.60](https://gitlab.com/Veroxis/jeeves/compare/1.5.59...1.5.60) (3/17/2024)


### Bug Fixes

* **deps:** update rust crate async-trait to 0.1.78 ([a8e5b82](https://gitlab.com/Veroxis/jeeves/commit/a8e5b82119b1e2985df46dec19fd828e2991196c))

## [1.5.59](https://gitlab.com/Veroxis/jeeves/compare/1.5.58...1.5.59) (3/15/2024)


### Bug Fixes

* **deps:** update rust crate clap to 4.5.3 ([b785caa](https://gitlab.com/Veroxis/jeeves/commit/b785caa04618f78c990f720f3d5a75d4a0cb0ac6))

## [1.5.58](https://gitlab.com/Veroxis/jeeves/compare/1.5.57...1.5.58) (3/15/2024)


### Bug Fixes

* **deps:** update rust crate color-eyre to 0.6.3 ([1409ea6](https://gitlab.com/Veroxis/jeeves/commit/1409ea65d4489a359cfe90de498ce530d2195c63))

## [1.5.57](https://gitlab.com/Veroxis/jeeves/compare/1.5.56...1.5.57) (3/13/2024)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.58 ([7fa47c5](https://gitlab.com/Veroxis/jeeves/commit/7fa47c535a3081f3cdacbfb1d7957aff7da9224c))

## [1.5.56](https://gitlab.com/Veroxis/jeeves/compare/1.5.55...1.5.56) (3/13/2024)


### Bug Fixes

* **deps:** lock file maintenance ([28492dc](https://gitlab.com/Veroxis/jeeves/commit/28492dc87c6b18988de527fffd68da06f276ca30))

## [1.5.55](https://gitlab.com/Veroxis/jeeves/compare/1.5.54...1.5.55) (3/6/2024)


### Bug Fixes

* **deps:** update rust crate clap to 4.5.2 ([a510404](https://gitlab.com/Veroxis/jeeves/commit/a510404840c3b8a396e2dd626b6798b5d447f9ea))

## [1.5.54](https://gitlab.com/Veroxis/jeeves/compare/1.5.53...1.5.54) (3/6/2024)


### Bug Fixes

* **deps:** update rust crate chrono to 0.4.35 ([075b4a2](https://gitlab.com/Veroxis/jeeves/commit/075b4a28d51b2808eb2432c20c28394a0332d040))

## [1.5.53](https://gitlab.com/Veroxis/jeeves/compare/1.5.52...1.5.53) (2/28/2024)


### Bug Fixes

* **deps:** lock file maintenance ([6869414](https://gitlab.com/Veroxis/jeeves/commit/68694140981fb54b2a85119cc655333572f6d5db))

## [1.5.52](https://gitlab.com/Veroxis/jeeves/compare/1.5.51...1.5.52) (2/27/2024)


### Bug Fixes

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.76.0 ([b2a25bb](https://gitlab.com/Veroxis/jeeves/commit/b2a25bbd1a80e0c62f7d84e64c6e250dd49b057a))

## [1.5.51](https://gitlab.com/Veroxis/jeeves/compare/1.5.50...1.5.51) (2/21/2024)


### Bug Fixes

* **deps:** lock file maintenance ([9ad4c60](https://gitlab.com/Veroxis/jeeves/commit/9ad4c60ea9cad881b7dbaee6278b43107bc95f00))

## [1.5.50](https://gitlab.com/Veroxis/jeeves/compare/1.5.49...1.5.50) (2/20/2024)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.114 ([528e05d](https://gitlab.com/Veroxis/jeeves/commit/528e05d9ef68b567524373a2cae16c3ea377d05a))

## [1.5.49](https://gitlab.com/Veroxis/jeeves/compare/1.5.48...1.5.49) (2/20/2024)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.197 ([88d8ca2](https://gitlab.com/Veroxis/jeeves/commit/88d8ca2f25d18c1b70f09c39d3e0ad5b050811ff))

## [1.5.48](https://gitlab.com/Veroxis/jeeves/compare/1.5.47...1.5.48) (2/19/2024)


### Bug Fixes

* **deps:** update rust crate semver to 1.0.22 ([c299c13](https://gitlab.com/Veroxis/jeeves/commit/c299c134b444559fd7d067362e9c6df2f146bca5))

## [1.5.47](https://gitlab.com/Veroxis/jeeves/compare/1.5.46...1.5.47) (2/16/2024)


### Bug Fixes

* **deps:** update rust crate clap to 4.5.1 ([7efcd81](https://gitlab.com/Veroxis/jeeves/commit/7efcd819f1c7736d2a614bb945c89bd4ee273d57))

## [1.5.46](https://gitlab.com/Veroxis/jeeves/compare/1.5.45...1.5.46) (2/14/2024)


### Bug Fixes

* **deps:** lock file maintenance ([9a7cdfb](https://gitlab.com/Veroxis/jeeves/commit/9a7cdfb788b78eb873e08acfb30f8a45d1433926))

## [1.5.45](https://gitlab.com/Veroxis/jeeves/compare/1.5.44...1.5.45) (2/11/2024)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.57 ([79aa01d](https://gitlab.com/Veroxis/jeeves/commit/79aa01d72bb8831ae632ab3155c5ae89a9d666aa))

## [1.5.44](https://gitlab.com/Veroxis/jeeves/compare/1.5.43...1.5.44) (2/11/2024)


### Bug Fixes

* **deps:** update rust crate chrono to 0.4.34 ([cbb4225](https://gitlab.com/Veroxis/jeeves/commit/cbb422517bc0bb3366a4d471e32117bf199cbff1))

## [1.5.43](https://gitlab.com/Veroxis/jeeves/compare/1.5.42...1.5.43) (2/8/2024)


### Bug Fixes

* **deps:** update rust crate clap to 4.5.0 ([9707aaf](https://gitlab.com/Veroxis/jeeves/commit/9707aaf16e013b5aa84138595b766a133e4b903a))

## [1.5.42](https://gitlab.com/Veroxis/jeeves/compare/1.5.41...1.5.42) (2/7/2024)


### Bug Fixes

* **deps:** update rust crate tokio to 1.36.0 ([3ddbebb](https://gitlab.com/Veroxis/jeeves/commit/3ddbebbceacd5b77f9987961df1f8be52dd0e131))

## [1.5.41](https://gitlab.com/Veroxis/jeeves/compare/1.5.40...1.5.41) (2/7/2024)


### Bug Fixes

* **deps:** lock file maintenance ([3ef7d66](https://gitlab.com/Veroxis/jeeves/commit/3ef7d6681ef0c14240d00bdf238b12207c1e60c0))

## [1.5.40](https://gitlab.com/Veroxis/jeeves/compare/1.5.39...1.5.40) (1/31/2024)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.113 ([ce51f91](https://gitlab.com/Veroxis/jeeves/commit/ce51f9190e585010a2f1c772e6ecdeef7d02db79))

## [1.5.39](https://gitlab.com/Veroxis/jeeves/compare/1.5.38...1.5.39) (1/31/2024)


### Bug Fixes

* **deps:** update rust crate eyre to 0.6.12 ([c890dd8](https://gitlab.com/Veroxis/jeeves/commit/c890dd8d2f7e6377854608ce5305d962649dd8f8))

## [1.5.38](https://gitlab.com/Veroxis/jeeves/compare/1.5.37...1.5.38) (1/27/2024)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.112 ([5854d68](https://gitlab.com/Veroxis/jeeves/commit/5854d68537d83b323f4acaef345d3b3641bfe218))

## [1.5.37](https://gitlab.com/Veroxis/jeeves/compare/1.5.36...1.5.37) (1/27/2024)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.196 ([06c2b51](https://gitlab.com/Veroxis/jeeves/commit/06c2b51fe82141e3ce09ca7210ec3c398642cc52))

## [1.5.36](https://gitlab.com/Veroxis/jeeves/compare/1.5.35...1.5.36) (1/25/2024)


### Bug Fixes

* **deps:** update rust crate chrono to 0.4.33 ([523d98f](https://gitlab.com/Veroxis/jeeves/commit/523d98f0393570bea4fb195578677c7b32459406))

## [1.5.35](https://gitlab.com/Veroxis/jeeves/compare/1.5.34...1.5.35) (1/24/2024)


### Bug Fixes

* **deps:** lock file maintenance ([aa07d43](https://gitlab.com/Veroxis/jeeves/commit/aa07d43ac205a75da4fe041958b1333451e07bc8))

## [1.5.34](https://gitlab.com/Veroxis/jeeves/compare/1.5.33...1.5.34) (1/23/2024)


### Bug Fixes

* **deps:** update rust crate chrono to 0.4.32 ([6ff556e](https://gitlab.com/Veroxis/jeeves/commit/6ff556e9458b723a60efdabd58ee49586b3c268d))

## [1.5.33](https://gitlab.com/Veroxis/jeeves/compare/1.5.32...1.5.33) (1/17/2024)


### Bug Fixes

* **deps:** lock file maintenance ([e3e1475](https://gitlab.com/Veroxis/jeeves/commit/e3e1475069fa853b427f1d0a53e1373726d2f759))

## [1.5.32](https://gitlab.com/Veroxis/jeeves/compare/1.5.31...1.5.32) (1/16/2024)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.18 ([4b93a36](https://gitlab.com/Veroxis/jeeves/commit/4b93a36c6e05b2f1d81547d2da1b47244dfd58bb))

## [1.5.31](https://gitlab.com/Veroxis/jeeves/compare/1.5.30...1.5.31) (1/15/2024)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.17 ([635d99f](https://gitlab.com/Veroxis/jeeves/commit/635d99f4d0cf5f9c8c154ff5ab0f24b6d3590297))

## [1.5.30](https://gitlab.com/Veroxis/jeeves/compare/1.5.29...1.5.30) (1/12/2024)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.16 ([ddedf1c](https://gitlab.com/Veroxis/jeeves/commit/ddedf1c81b2f9e53719708bf79ddc10bc7261adc))

## [1.5.29](https://gitlab.com/Veroxis/jeeves/compare/1.5.28...1.5.29) (1/11/2024)


### Bug Fixes

* **deps:** lock file maintenance ([9e5d7a7](https://gitlab.com/Veroxis/jeeves/commit/9e5d7a7d773bde4bdc6bd6a6aa6c425352166f68))

## [1.5.28](https://gitlab.com/Veroxis/jeeves/compare/1.5.27...1.5.28) (1/11/2024)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.15 ([2342dfa](https://gitlab.com/Veroxis/jeeves/commit/2342dfac70971e8d66646a86dbdc879bee76cf5a))

## [1.5.27](https://gitlab.com/Veroxis/jeeves/compare/1.5.26...1.5.27) (1/8/2024)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.14 ([6c644bb](https://gitlab.com/Veroxis/jeeves/commit/6c644bbf9067671d76988071da562a2e1274007b))

## [1.5.26](https://gitlab.com/Veroxis/jeeves/compare/1.5.25...1.5.26) (1/6/2024)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.195 ([834747b](https://gitlab.com/Veroxis/jeeves/commit/834747b6372a3d0fd95b18d0c85ce4c0d1f91c49))

## [1.5.25](https://gitlab.com/Veroxis/jeeves/compare/1.5.24...1.5.25) (1/4/2024)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.13 ([5e06892](https://gitlab.com/Veroxis/jeeves/commit/5e06892f993dfdbf221dffcddf55c574cc7c17bf))

## [1.5.24](https://gitlab.com/Veroxis/jeeves/compare/1.5.23...1.5.24) (1/4/2024)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.111 ([b342c2c](https://gitlab.com/Veroxis/jeeves/commit/b342c2c1b9502ce48710e7c3117d558e7bc9a387))

## [1.5.23](https://gitlab.com/Veroxis/jeeves/compare/1.5.22...1.5.23) (1/3/2024)


### Bug Fixes

* **deps:** lock file maintenance ([93366b4](https://gitlab.com/Veroxis/jeeves/commit/93366b4888c3f03ec6fb628a21253d1cb0a7d1d2))

## [1.5.22](https://gitlab.com/Veroxis/jeeves/compare/1.5.21...1.5.22) (1/2/2024)


### Bug Fixes

* **deps:** update rust crate semver to 1.0.21 ([fa23422](https://gitlab.com/Veroxis/jeeves/commit/fa2342283c04e82e61de650a969144026c717537))

## [1.5.21](https://gitlab.com/Veroxis/jeeves/compare/1.5.20...1.5.21) (1/2/2024)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.56 ([2adf092](https://gitlab.com/Veroxis/jeeves/commit/2adf092d1db496802d3a750da30a3ef22dac6095))

## [1.5.20](https://gitlab.com/Veroxis/jeeves/compare/1.5.19...1.5.20) (1/2/2024)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.110 ([0ff10d2](https://gitlab.com/Veroxis/jeeves/commit/0ff10d2b3e4506732d9b75ede26c68bc71e08a10))

## [1.5.19](https://gitlab.com/Veroxis/jeeves/compare/1.5.18...1.5.19) (1/2/2024)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.194 ([41f1043](https://gitlab.com/Veroxis/jeeves/commit/41f1043a44cc60a9f680c71350e1b6714e0795b1))

## [1.5.18](https://gitlab.com/Veroxis/jeeves/compare/1.5.17...1.5.18) (1/2/2024)


### Bug Fixes

* **deps:** update rust crate async-trait to 0.1.77 ([ba73d66](https://gitlab.com/Veroxis/jeeves/commit/ba73d66af1748bd4038c24546a8d82bf92d68293))

## [1.5.17](https://gitlab.com/Veroxis/jeeves/compare/1.5.16...1.5.17) (1/1/2024)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.109 ([8b2c960](https://gitlab.com/Veroxis/jeeves/commit/8b2c960306161d7a7802b730779a5f7e34ca95e0))

## [1.5.16](https://gitlab.com/Veroxis/jeeves/compare/1.5.15...1.5.16) (12/31/2023)


### Bug Fixes

* **deps:** update rust crate async-trait to 0.1.76 ([1f99efc](https://gitlab.com/Veroxis/jeeves/commit/1f99efccb518b84ce015e854790c3ee8fb752ec1))

## [1.5.15](https://gitlab.com/Veroxis/jeeves/compare/1.5.14...1.5.15) (12/30/2023)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.53 ([9f08578](https://gitlab.com/Veroxis/jeeves/commit/9f08578fcfad06fc469fc32f3bfe147c8a2e363d))

## [1.5.14](https://gitlab.com/Veroxis/jeeves/compare/1.5.13...1.5.14) (12/28/2023)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.12 ([209ecf6](https://gitlab.com/Veroxis/jeeves/commit/209ecf649caa0580763bb274f5d620104264a40a))

## [1.5.13](https://gitlab.com/Veroxis/jeeves/compare/1.5.12...1.5.13) (12/27/2023)


### Bug Fixes

* **deps:** lock file maintenance ([4772c53](https://gitlab.com/Veroxis/jeeves/commit/4772c53e7e27b7ead128e98f49f6488eba59b8ca))

## [1.5.12](https://gitlab.com/Veroxis/jeeves/compare/1.5.11...1.5.12) (12/25/2023)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.52 ([427d028](https://gitlab.com/Veroxis/jeeves/commit/427d028d520fd5841ada31098bb251cd4580b7ed))

## [1.5.11](https://gitlab.com/Veroxis/jeeves/compare/1.5.10...1.5.11) (12/24/2023)


### Bug Fixes

* **deps:** update rust crate futures to 0.3.30 ([4732378](https://gitlab.com/Veroxis/jeeves/commit/4732378a4199a9f4f1aa664081b107558ca74bb1))

## [1.5.10](https://gitlab.com/Veroxis/jeeves/compare/1.5.9...1.5.10) (12/21/2023)


### Bug Fixes

* **deps:** update rust crate async-trait to 0.1.75 ([451a2f1](https://gitlab.com/Veroxis/jeeves/commit/451a2f154d313a2e2e327c96778265637f62c8c1))

## [1.5.9](https://gitlab.com/Veroxis/jeeves/compare/1.5.8...1.5.9) (12/20/2023)


### Bug Fixes

* **deps:** update rust crate tokio to 1.35.1 ([ee66933](https://gitlab.com/Veroxis/jeeves/commit/ee66933a8e570a112b502d7b102fc7f2a5458bd1))

## [1.5.8](https://gitlab.com/Veroxis/jeeves/compare/1.5.7...1.5.8) (12/20/2023)


### Bug Fixes

* **deps:** lock file maintenance ([e64e160](https://gitlab.com/Veroxis/jeeves/commit/e64e16035a5222c1487fc67276c96fff0e1247b0))

## [1.5.7](https://gitlab.com/Veroxis/jeeves/compare/1.5.6...1.5.7) (12/16/2023)


### Bug Fixes

* **deps:** update rust crate tokio to 1.35.0 ([8d57829](https://gitlab.com/Veroxis/jeeves/commit/8d578297c2acfd2fa04545cf4c61b809e4ce0766))

## [1.5.6](https://gitlab.com/Veroxis/jeeves/compare/1.5.5...1.5.6) (12/16/2023)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.51 ([b00c3ee](https://gitlab.com/Veroxis/jeeves/commit/b00c3ee7d47688197ac15467b53197df99009680))

## [1.5.5](https://gitlab.com/Veroxis/jeeves/compare/1.5.4...1.5.5) (12/16/2023)


### Bug Fixes

* **deps:** update rust crate eyre to 0.6.11 ([3a7b715](https://gitlab.com/Veroxis/jeeves/commit/3a7b715f28a25ecd2a769514451d76c809d158a7))

## [1.5.4](https://gitlab.com/Veroxis/jeeves/compare/1.5.3...1.5.4) (12/15/2023)


### Bug Fixes

* drop the --needed flag for AUR updates ([c2cb31d](https://gitlab.com/Veroxis/jeeves/commit/c2cb31deb0bcf7e2214e9c379fe30ad674c9f3a8))

## [1.5.3](https://gitlab.com/Veroxis/jeeves/compare/1.5.2...1.5.3) (12/8/2023)


### Bug Fixes

* **deps:** update rust crate eyre to 0.6.10 ([e7745f4](https://gitlab.com/Veroxis/jeeves/commit/e7745f40b33a69fae18feced416a74ee0a19cc7d))

## [1.5.2](https://gitlab.com/Veroxis/jeeves/compare/1.5.1...1.5.2) (12/8/2023)


### Bug Fixes

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.74.1 ([6feb6b5](https://gitlab.com/Veroxis/jeeves/commit/6feb6b53b81ea40692bde0eb281660d377facebb))

## [1.5.1](https://gitlab.com/Veroxis/jeeves/compare/1.5.0...1.5.1) (12/4/2023)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.11 ([b2f19bb](https://gitlab.com/Veroxis/jeeves/commit/b2f19bb110b85c0fcaf2211aa550170d72a60217))

# [1.5.0](https://gitlab.com/Veroxis/jeeves/compare/1.4.165...1.5.0) (12/4/2023)


### Features

* update flatpak '--user' packages ([92f01c0](https://gitlab.com/Veroxis/jeeves/commit/92f01c02e45d33c95b389fed54f017fcda804ae8))

## [1.4.165](https://gitlab.com/Veroxis/jeeves/compare/1.4.164...1.4.165) (11/29/2023)


### Bug Fixes

* **deps:** lock file maintenance ([1b0a3c4](https://gitlab.com/Veroxis/jeeves/commit/1b0a3c4e3c514fb0d279ce088a0f21bce9f6ed9d))

## [1.4.164](https://gitlab.com/Veroxis/jeeves/compare/1.4.163...1.4.164) (11/28/2023)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.10 ([c232305](https://gitlab.com/Veroxis/jeeves/commit/c232305e5dce6b0420a5555565e16bef358f1643))

## [1.4.163](https://gitlab.com/Veroxis/jeeves/compare/1.4.162...1.4.163) (11/27/2023)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.9 ([a03f671](https://gitlab.com/Veroxis/jeeves/commit/a03f671e082e5ea37efc0c282cdf77dd83bd6816))

## [1.4.162](https://gitlab.com/Veroxis/jeeves/compare/1.4.161...1.4.162) (11/21/2023)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.193 ([216e82b](https://gitlab.com/Veroxis/jeeves/commit/216e82b32aeef750bb0987f9d34b4f6d0efc0cf3))

## [1.4.161](https://gitlab.com/Veroxis/jeeves/compare/1.4.160...1.4.161) (11/20/2023)


### Bug Fixes

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.74.0 ([b206ae7](https://gitlab.com/Veroxis/jeeves/commit/b206ae713a0bcb95d8b36e02dc05a8f758738eea))

## [1.4.160](https://gitlab.com/Veroxis/jeeves/compare/1.4.159...1.4.160) (11/17/2023)


### Bug Fixes

* **deps:** update rust crate eyre to 0.6.9 ([d832331](https://gitlab.com/Veroxis/jeeves/commit/d832331cd8e470aea67d94b8da7ea1ce625f52b0))

## [1.4.159](https://gitlab.com/Veroxis/jeeves/compare/1.4.158...1.4.159) (11/15/2023)


### Bug Fixes

* **deps:** lock file maintenance ([f3e8d14](https://gitlab.com/Veroxis/jeeves/commit/f3e8d14304573f1bbb8ce66afed058a82bb1d335))

## [1.4.158](https://gitlab.com/Veroxis/jeeves/compare/1.4.157...1.4.158) (11/10/2023)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.8 ([a5f1603](https://gitlab.com/Veroxis/jeeves/commit/a5f16035dc644253be160d2ae1abaa8015748f5e))

## [1.4.157](https://gitlab.com/Veroxis/jeeves/compare/1.4.156...1.4.157) (11/9/2023)


### Bug Fixes

* **deps:** update rust crate tokio to 1.34.0 ([0372a9d](https://gitlab.com/Veroxis/jeeves/commit/0372a9d17f3f18f9580b4b22dffd59316e28c359))

## [1.4.156](https://gitlab.com/Veroxis/jeeves/compare/1.4.155...1.4.156) (11/8/2023)


### Bug Fixes

* **deps:** lock file maintenance ([9e31ce7](https://gitlab.com/Veroxis/jeeves/commit/9e31ce7d3e086f711f536b7735058bcc6c67e94b))

## [1.4.155](https://gitlab.com/Veroxis/jeeves/compare/1.4.154...1.4.155) (11/7/2023)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.192 ([5b1f375](https://gitlab.com/Veroxis/jeeves/commit/5b1f375458e4fe83154e5cfa9c58d01e935777f4))

## [1.4.154](https://gitlab.com/Veroxis/jeeves/compare/1.4.153...1.4.154) (11/6/2023)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.191 ([0ca5cfb](https://gitlab.com/Veroxis/jeeves/commit/0ca5cfbf1487fb59008bdc7ceb496c96aea7c36e))

## [1.4.153](https://gitlab.com/Veroxis/jeeves/compare/1.4.152...1.4.153) (10/30/2023)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.108 ([2ba2401](https://gitlab.com/Veroxis/jeeves/commit/2ba2401959572d82c70e000a80e56567b45f0753))

## [1.4.152](https://gitlab.com/Veroxis/jeeves/compare/1.4.151...1.4.152) (10/26/2023)


### Bug Fixes

* **deps:** update rust crate futures to 0.3.29 ([a5e7156](https://gitlab.com/Veroxis/jeeves/commit/a5e71564a4f2ca550491f66883d786953e8901c9))

## [1.4.151](https://gitlab.com/Veroxis/jeeves/compare/1.4.150...1.4.151) (10/26/2023)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.190 ([a9ac59a](https://gitlab.com/Veroxis/jeeves/commit/a9ac59aee0d8da4fe39d9aad37e16d7c8a0cc59c))

## [1.4.150](https://gitlab.com/Veroxis/jeeves/compare/1.4.149...1.4.150) (10/25/2023)


### Bug Fixes

* **deps:** lock file maintenance ([6d4bc89](https://gitlab.com/Veroxis/jeeves/commit/6d4bc89c1ca4ac3e2cea2a04d77d4fed99a3a60d))

## [1.4.149](https://gitlab.com/Veroxis/jeeves/compare/1.4.148...1.4.149) (10/24/2023)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.7 ([7fa1ec9](https://gitlab.com/Veroxis/jeeves/commit/7fa1ec960a819ee216d6235dfd620a497add8ab6))

## [1.4.148](https://gitlab.com/Veroxis/jeeves/compare/1.4.147...1.4.148) (10/19/2023)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.50 ([82dbb84](https://gitlab.com/Veroxis/jeeves/commit/82dbb84e2413ca4794a7d8766761772f83333997))

## [1.4.147](https://gitlab.com/Veroxis/jeeves/compare/1.4.146...1.4.147) (10/18/2023)


### Bug Fixes

* **deps:** lock file maintenance ([f716fd2](https://gitlab.com/Veroxis/jeeves/commit/f716fd2c7213f92659e0df2a6c5d40a498616df8))

## [1.4.146](https://gitlab.com/Veroxis/jeeves/compare/1.4.145...1.4.146) (10/17/2023)


### Bug Fixes

* **deps:** update rust crate minreq to 2.11.0 ([02f22d1](https://gitlab.com/Veroxis/jeeves/commit/02f22d15b6597f232af57414e0106c80f5dd236c))

## [1.4.145](https://gitlab.com/Veroxis/jeeves/compare/1.4.144...1.4.145) (10/15/2023)


### Bug Fixes

* **deps:** update rust crate async-trait to 0.1.74 ([f813406](https://gitlab.com/Veroxis/jeeves/commit/f8134065c232a55be0252bdcbfe740c5c343d8d1))

## [1.4.144](https://gitlab.com/Veroxis/jeeves/compare/1.4.143...1.4.144) (10/13/2023)


### Bug Fixes

* **deps:** update rust crate flate2 to 1.0.28 ([3aefdfd](https://gitlab.com/Veroxis/jeeves/commit/3aefdfd3629c0ae0c1ad8a45e85e064e33afa08f))

## [1.4.143](https://gitlab.com/Veroxis/jeeves/compare/1.4.142...1.4.143) (10/13/2023)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.189 ([82bbc46](https://gitlab.com/Veroxis/jeeves/commit/82bbc4627e31a0fa728f04cc9cb6dcc623d9e92e))

## [1.4.142](https://gitlab.com/Veroxis/jeeves/compare/1.4.141...1.4.142) (10/11/2023)


### Bug Fixes

* **deps:** lock file maintenance ([f7a028f](https://gitlab.com/Veroxis/jeeves/commit/f7a028f74e65cf244af64c1d9ea733a95e0f593a))

## [1.4.141](https://gitlab.com/Veroxis/jeeves/compare/1.4.140...1.4.141) (10/10/2023)


### Bug Fixes

* optimize usage of Option<T> ([8ca8cbd](https://gitlab.com/Veroxis/jeeves/commit/8ca8cbddcbba85a4abd65476e25c334c83083f3d))

## [1.4.140](https://gitlab.com/Veroxis/jeeves/compare/1.4.139...1.4.140) (10/10/2023)


### Bug Fixes

* **deps:** update rust crate semver to 1.0.20 ([bd7d4ec](https://gitlab.com/Veroxis/jeeves/commit/bd7d4ec1adc8c26f999e634b1c9e23c1c2fca98f))

## [1.4.139](https://gitlab.com/Veroxis/jeeves/compare/1.4.138...1.4.139) (10/9/2023)


### Bug Fixes

* **deps:** update rust crate tokio to 1.33.0 ([5b9a436](https://gitlab.com/Veroxis/jeeves/commit/5b9a436b000bc99a5a9f1605ac54cbba2ec0f4c0))

## [1.4.138](https://gitlab.com/Veroxis/jeeves/compare/1.4.137...1.4.138) (10/6/2023)


### Bug Fixes

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.73.0 ([b9b1ce7](https://gitlab.com/Veroxis/jeeves/commit/b9b1ce75aa32874ba715ef7d72539fe03fec7c48))

## [1.4.137](https://gitlab.com/Veroxis/jeeves/compare/1.4.136...1.4.137) (10/4/2023)


### Bug Fixes

* **deps:** lock file maintenance ([7062e9c](https://gitlab.com/Veroxis/jeeves/commit/7062e9c1547a9f230164650a1d0c92d5168e82d5))

## [1.4.136](https://gitlab.com/Veroxis/jeeves/compare/1.4.135...1.4.136) (9/28/2023)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.6 ([4a4346d](https://gitlab.com/Veroxis/jeeves/commit/4a4346df6e24ef4e80081eac4b616ad554804423))

## [1.4.135](https://gitlab.com/Veroxis/jeeves/compare/1.4.134...1.4.135) (9/28/2023)


### Bug Fixes

* **deps:** lock file maintenance ([3088998](https://gitlab.com/Veroxis/jeeves/commit/3088998b7144eab6142f09a31e773811d5e2b220))

## [1.4.134](https://gitlab.com/Veroxis/jeeves/compare/1.4.133...1.4.134) (9/27/2023)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.49 ([668eb3d](https://gitlab.com/Veroxis/jeeves/commit/668eb3d456f8c7c88938539f91a6631f9d2bb253))

## [1.4.133](https://gitlab.com/Veroxis/jeeves/compare/1.4.132...1.4.133) (9/25/2023)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.5 ([5b0ec99](https://gitlab.com/Veroxis/jeeves/commit/5b0ec99898d2b2dc628f15bf1bff9ac5dcadc3b0))

## [1.4.132](https://gitlab.com/Veroxis/jeeves/compare/1.4.131...1.4.132) (9/24/2023)


### Bug Fixes

* **deps:** lock file maintenance ([3598b83](https://gitlab.com/Veroxis/jeeves/commit/3598b83b19de6e1059a0c417143ae6b281c9138a))

## [1.4.131](https://gitlab.com/Veroxis/jeeves/compare/1.4.130...1.4.131) (9/22/2023)


### Bug Fixes

* **deps:** update rust crate semver to 1.0.19 ([6587bf7](https://gitlab.com/Veroxis/jeeves/commit/6587bf7f5bdc93add41797e810b93ca7766d05b0))

## [1.4.130](https://gitlab.com/Veroxis/jeeves/compare/1.4.129...1.4.130) (9/22/2023)


### Bug Fixes

* **deps:** lock file maintenance ([48db48f](https://gitlab.com/Veroxis/jeeves/commit/48db48fd4855eac0bb55369cb176352b42e9aae4))

## [1.4.129](https://gitlab.com/Veroxis/jeeves/compare/1.4.128...1.4.129) (9/21/2023)


### Bug Fixes

* **deps:** lock file maintenance ([937a785](https://gitlab.com/Veroxis/jeeves/commit/937a7852175a961de59a4e55361e62d57e051aae))

## [1.4.128](https://gitlab.com/Veroxis/jeeves/compare/1.4.127...1.4.128) (9/20/2023)


### Bug Fixes

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.72.1 ([f757a01](https://gitlab.com/Veroxis/jeeves/commit/f757a019f04f4f4e11b06ed883fd59e4385a1cfe))

## [1.4.127](https://gitlab.com/Veroxis/jeeves/compare/1.4.126...1.4.127) (9/20/2023)


### Bug Fixes

* **deps:** lock file maintenance ([9a3b0a7](https://gitlab.com/Veroxis/jeeves/commit/9a3b0a70dcabe56e2426ed740b3ce74f4583f869))

## [1.4.126](https://gitlab.com/Veroxis/jeeves/compare/1.4.125...1.4.126) (9/19/2023)


### Bug Fixes

* **deps:** lock file maintenance ([b1b26bf](https://gitlab.com/Veroxis/jeeves/commit/b1b26bff38b379307cab7af7efdf5b24c78b6397))

## [1.4.125](https://gitlab.com/Veroxis/jeeves/compare/1.4.124...1.4.125) (9/18/2023)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.4 ([90ac039](https://gitlab.com/Veroxis/jeeves/commit/90ac0395d60cc1ab6904ac986d4e46b60ba73f1f))

## [1.4.124](https://gitlab.com/Veroxis/jeeves/compare/1.4.123...1.4.124) (9/18/2023)


### Bug Fixes

* **deps:** lock file maintenance ([61929d0](https://gitlab.com/Veroxis/jeeves/commit/61929d096f5a76d326eb0e4351629aaface85552))

## [1.4.123](https://gitlab.com/Veroxis/jeeves/compare/1.4.122...1.4.123) (9/17/2023)


### Bug Fixes

* **deps:** lock file maintenance ([360ad96](https://gitlab.com/Veroxis/jeeves/commit/360ad968fd4723a1574e1f72a5af6139a3f3b02a))

## [1.4.122](https://gitlab.com/Veroxis/jeeves/compare/1.4.121...1.4.122) (2023-09-16)


### Bug Fixes

* **deps:** lock file maintenance ([fdb2298](https://gitlab.com/Veroxis/jeeves/commit/fdb22981281c06133e3a353b70f47ef862533a71))

## [1.4.121](https://gitlab.com/Veroxis/jeeves/compare/1.4.120...1.4.121) (2023-09-15)


### Bug Fixes

* **deps:** update rust crate chrono to 0.4.31 ([19d5804](https://gitlab.com/Veroxis/jeeves/commit/19d5804678b03c994e41966c411c0724053c8240))

## [1.4.120](https://gitlab.com/Veroxis/jeeves/compare/1.4.119...1.4.120) (2023-09-15)


### Bug Fixes

* **deps:** lock file maintenance ([8e40fa5](https://gitlab.com/Veroxis/jeeves/commit/8e40fa565c14da80366dd6ebfdaee96dd4fe55fe))

## [1.4.119](https://gitlab.com/Veroxis/jeeves/compare/1.4.118...1.4.119) (2023-09-14)


### Bug Fixes

* **deps:** lock file maintenance ([a410072](https://gitlab.com/Veroxis/jeeves/commit/a4100722a2132421c940895e0fa168a5fd4782d5))

## [1.4.118](https://gitlab.com/Veroxis/jeeves/compare/1.4.117...1.4.118) (2023-09-14)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.107 ([24379e6](https://gitlab.com/Veroxis/jeeves/commit/24379e6efe79aea998cbc03402a5ae602e897870))

## [1.4.117](https://gitlab.com/Veroxis/jeeves/compare/1.4.116...1.4.117) (2023-09-13)


### Bug Fixes

* **deps:** lock file maintenance ([378079e](https://gitlab.com/Veroxis/jeeves/commit/378079e9bc68f1164c3c78152bbd1de3bcc223c9))

## [1.4.116](https://gitlab.com/Veroxis/jeeves/compare/1.4.115...1.4.116) (2023-09-13)


### Bug Fixes

* **deps:** lock file maintenance ([f72360d](https://gitlab.com/Veroxis/jeeves/commit/f72360db2d45dd12bc637d0e90868692b0d6ef99))

## [1.4.115](https://gitlab.com/Veroxis/jeeves/compare/1.4.114...1.4.115) (2023-09-12)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.3 ([7f7e7d4](https://gitlab.com/Veroxis/jeeves/commit/7f7e7d47c395e4c0d7dd2ce8c8a58811129bb856))

## [1.4.114](https://gitlab.com/Veroxis/jeeves/compare/1.4.113...1.4.114) (2023-09-12)


### Bug Fixes

* **deps:** lock file maintenance ([f50d74f](https://gitlab.com/Veroxis/jeeves/commit/f50d74f63a726d0f1e9037499d497b10c9b3e83f))

## [1.4.113](https://gitlab.com/Veroxis/jeeves/compare/1.4.112...1.4.113) (2023-09-11)


### Bug Fixes

* **deps:** lock file maintenance ([ab3451d](https://gitlab.com/Veroxis/jeeves/commit/ab3451de6548f612e2495eb9b195fc6bdb327b08))

## [1.4.112](https://gitlab.com/Veroxis/jeeves/compare/1.4.111...1.4.112) (2023-09-09)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.106 ([9cd7895](https://gitlab.com/Veroxis/jeeves/commit/9cd7895581813c7497b7cc108aa18abfd320eddf))

## [1.4.111](https://gitlab.com/Veroxis/jeeves/compare/1.4.110...1.4.111) (2023-09-08)


### Bug Fixes

* **deps:** lock file maintenance ([fc8eff2](https://gitlab.com/Veroxis/jeeves/commit/fc8eff2bc4812af580205e531daf9bf6c43bbd8b))

## [1.4.110](https://gitlab.com/Veroxis/jeeves/compare/1.4.109...1.4.110) (2023-09-07)


### Bug Fixes

* **deps:** update rust crate chrono to 0.4.30 ([d829978](https://gitlab.com/Veroxis/jeeves/commit/d829978df8de06f70018b8748665016fc6b47f68))

## [1.4.109](https://gitlab.com/Veroxis/jeeves/compare/1.4.108...1.4.109) (2023-09-05)


### Bug Fixes

* **deps:** update rust crate minreq to 2.10.0 ([a6c7f41](https://gitlab.com/Veroxis/jeeves/commit/a6c7f4143110f2fc1654db9b3ec59682ad3fd882))

## [1.4.108](https://gitlab.com/Veroxis/jeeves/compare/1.4.107...1.4.108) (2023-09-05)


### Bug Fixes

* **deps:** update rust crate chrono to 0.4.29 ([eefc98a](https://gitlab.com/Veroxis/jeeves/commit/eefc98ac6cc9e357dbeff7d376cd6429cdde962d))

## [1.4.107](https://gitlab.com/Veroxis/jeeves/compare/1.4.106...1.4.107) (2023-09-05)


### Bug Fixes

* **deps:** lock file maintenance ([ef3ebc6](https://gitlab.com/Veroxis/jeeves/commit/ef3ebc6108a1b117b6c28a00354d2c46241088a6))

## [1.4.106](https://gitlab.com/Veroxis/jeeves/compare/1.4.105...1.4.106) (2023-09-04)


### Bug Fixes

* **deps:** lock file maintenance ([60aca79](https://gitlab.com/Veroxis/jeeves/commit/60aca79e94b47d21a10241551c27ff94408769b0))

## [1.4.105](https://gitlab.com/Veroxis/jeeves/compare/1.4.104...1.4.105) (2023-09-03)


### Bug Fixes

* **deps:** lock file maintenance ([d591857](https://gitlab.com/Veroxis/jeeves/commit/d5918573a97646bda48161007a87cb26f5640880))

## [1.4.104](https://gitlab.com/Veroxis/jeeves/compare/1.4.103...1.4.104) (2023-09-03)


### Bug Fixes

* **deps:** lock file maintenance ([69bac94](https://gitlab.com/Veroxis/jeeves/commit/69bac94fba7cc00644246871c6f0605cf30d8263))

## [1.4.103](https://gitlab.com/Veroxis/jeeves/compare/1.4.102...1.4.103) (2023-09-02)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.48 ([8222aab](https://gitlab.com/Veroxis/jeeves/commit/8222aab7659ec33271d3deaba5637ac2cf5926ea))

## [1.4.102](https://gitlab.com/Veroxis/jeeves/compare/1.4.101...1.4.102) (2023-08-31)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.2 ([0c20552](https://gitlab.com/Veroxis/jeeves/commit/0c205521ce391085c15e4ba2a04dc92a8c949ce6))

## [1.4.101](https://gitlab.com/Veroxis/jeeves/compare/1.4.100...1.4.101) (2023-08-31)


### Bug Fixes

* **deps:** lock file maintenance ([050ae1f](https://gitlab.com/Veroxis/jeeves/commit/050ae1f3da5b6f6524fd05330e87994aa1d90f2a))

## [1.4.100](https://gitlab.com/Veroxis/jeeves/compare/1.4.99...1.4.100) (2023-08-30)


### Bug Fixes

* **deps:** update rust crate chrono to 0.4.28 ([007ebe9](https://gitlab.com/Veroxis/jeeves/commit/007ebe9bf89277801e8d25a6f3ea8f92cec4a574))

## [1.4.99](https://gitlab.com/Veroxis/jeeves/compare/1.4.98...1.4.99) (2023-08-30)


### Bug Fixes

* **deps:** lock file maintenance ([83176d0](https://gitlab.com/Veroxis/jeeves/commit/83176d087acc050ba69f2fc52a69b6ace87efa81))

## [1.4.98](https://gitlab.com/Veroxis/jeeves/compare/1.4.97...1.4.98) (2023-08-29)


### Bug Fixes

* **deps:** update rust crate chrono to 0.4.27 ([8bb254d](https://gitlab.com/Veroxis/jeeves/commit/8bb254dcf338f77e2b52e1037502aa2d2dca9002))

## [1.4.97](https://gitlab.com/Veroxis/jeeves/compare/1.4.96...1.4.97) (2023-08-29)


### Bug Fixes

* **deps:** lock file maintenance ([01e08e4](https://gitlab.com/Veroxis/jeeves/commit/01e08e423290e35de00ca15b7622e8845eccbd73))

## [1.4.96](https://gitlab.com/Veroxis/jeeves/compare/1.4.95...1.4.96) (2023-08-28)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.1 ([b5c238f](https://gitlab.com/Veroxis/jeeves/commit/b5c238f295d60bc35b9935d7aeb245caab1e1eb0))

## [1.4.95](https://gitlab.com/Veroxis/jeeves/compare/1.4.94...1.4.95) (2023-08-28)


### Bug Fixes

* **deps:** update rust crate minreq to 2.9.1 ([32c8731](https://gitlab.com/Veroxis/jeeves/commit/32c87316d3eebe94406f86ed356e7c690da62d23))

## [1.4.94](https://gitlab.com/Veroxis/jeeves/compare/1.4.93...1.4.94) (2023-08-28)


### Bug Fixes

* **deps:** lock file maintenance ([c716483](https://gitlab.com/Veroxis/jeeves/commit/c716483dedac2601d3b7a1d80fd9c8178810d125))

## [1.4.93](https://gitlab.com/Veroxis/jeeves/compare/1.4.92...1.4.93) (2023-08-27)


### Bug Fixes

* **deps:** lock file maintenance ([83f9dce](https://gitlab.com/Veroxis/jeeves/commit/83f9dce8966beb790d21263681cb6ee95ec5df3d))

## [1.4.92](https://gitlab.com/Veroxis/jeeves/compare/1.4.91...1.4.92) (2023-08-26)


### Bug Fixes

* **deps:** lock file maintenance ([01d84ab](https://gitlab.com/Veroxis/jeeves/commit/01d84abf86674429c9f246f213a6d2ffd74082a2))

## [1.4.91](https://gitlab.com/Veroxis/jeeves/compare/1.4.90...1.4.91) (2023-08-26)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.188 ([103e22d](https://gitlab.com/Veroxis/jeeves/commit/103e22dceb6c25ba15dc35ce9d8fd3ab954f8580))

## [1.4.90](https://gitlab.com/Veroxis/jeeves/compare/1.4.89...1.4.90) (2023-08-25)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.187 ([9b5c359](https://gitlab.com/Veroxis/jeeves/commit/9b5c359006ca6bc60cfb518e723e4ac8c0b00311))

## [1.4.89](https://gitlab.com/Veroxis/jeeves/compare/1.4.88...1.4.89) (2023-08-25)


### Bug Fixes

* **deps:** lock file maintenance ([8a87eee](https://gitlab.com/Veroxis/jeeves/commit/8a87eeef3cc274b35b6e8ac1a0523c8dd8d31f52))

## [1.4.88](https://gitlab.com/Veroxis/jeeves/compare/1.4.87...1.4.88) (2023-08-25)


### Bug Fixes

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.72.0 ([c185d09](https://gitlab.com/Veroxis/jeeves/commit/c185d0937ba6c3f589c744fadaecb8101f520ae0))

## [1.4.87](https://gitlab.com/Veroxis/jeeves/compare/1.4.86...1.4.87) (2023-08-24)


### Bug Fixes

* **deps:** update rust crate minreq to 2.9.0 ([49e70b5](https://gitlab.com/Veroxis/jeeves/commit/49e70b5df187408e05976892b9003fb2b2037a9a))

## [1.4.86](https://gitlab.com/Veroxis/jeeves/compare/1.4.85...1.4.86) (2023-08-24)


### Bug Fixes

* **deps:** update rust crate clap to 4.4.0 ([0222529](https://gitlab.com/Veroxis/jeeves/commit/0222529e49f3e88e89f1e2e446450e407bce024c))

## [1.4.85](https://gitlab.com/Veroxis/jeeves/compare/1.4.84...1.4.85) (2023-08-24)


### Bug Fixes

* **deps:** lock file maintenance ([000abb7](https://gitlab.com/Veroxis/jeeves/commit/000abb72a29543898674b0be27458dc76504d327))

## [1.4.84](https://gitlab.com/Veroxis/jeeves/compare/1.4.83...1.4.84) (2023-08-24)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.186 ([15f0334](https://gitlab.com/Veroxis/jeeves/commit/15f0334c2bfcb1e9af9a8b95118d3d82e5adfd2e))

## [1.4.83](https://gitlab.com/Veroxis/jeeves/compare/1.4.82...1.4.83) (2023-08-23)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.24 ([8c01591](https://gitlab.com/Veroxis/jeeves/commit/8c01591eed3c1ef27d9d6dab1b7810ffeb1395a0))

## [1.4.82](https://gitlab.com/Veroxis/jeeves/compare/1.4.81...1.4.82) (2023-08-23)


### Bug Fixes

* **deps:** lock file maintenance ([7d8a90c](https://gitlab.com/Veroxis/jeeves/commit/7d8a90c784aba1b83d5f60eb147e8c38baa532c8))

## [1.4.81](https://gitlab.com/Veroxis/jeeves/compare/1.4.80...1.4.81) (2023-08-21)


### Bug Fixes

* **deps:** lock file maintenance ([efe84e5](https://gitlab.com/Veroxis/jeeves/commit/efe84e54055e1f2a2528a289ead349f0e0facc29))

## [1.4.80](https://gitlab.com/Veroxis/jeeves/compare/1.4.79...1.4.80) (2023-08-21)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.185 ([2f9afcb](https://gitlab.com/Veroxis/jeeves/commit/2f9afcba8c303c6bf404d86dc98c96e6d86cf01e))

## [1.4.79](https://gitlab.com/Veroxis/jeeves/compare/1.4.78...1.4.79) (2023-08-21)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.184 ([8e6f949](https://gitlab.com/Veroxis/jeeves/commit/8e6f949c3c584638b342b6b47963a6a714a75d92))

## [1.4.78](https://gitlab.com/Veroxis/jeeves/compare/1.4.77...1.4.78) (2023-08-20)


### Bug Fixes

* **deps:** lock file maintenance ([a229678](https://gitlab.com/Veroxis/jeeves/commit/a22967870922f2f49a4d69387749cda74bc8f434))

## [1.4.77](https://gitlab.com/Veroxis/jeeves/compare/1.4.76...1.4.77) (2023-08-19)


### Bug Fixes

* **deps:** lock file maintenance ([1db3935](https://gitlab.com/Veroxis/jeeves/commit/1db3935e3978c39c08dc17c08f42445eccfe886e))

## [1.4.76](https://gitlab.com/Veroxis/jeeves/compare/1.4.75...1.4.76) (2023-08-18)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.23 ([3246343](https://gitlab.com/Veroxis/jeeves/commit/32463439f9c07e5f8b3b2497169bb7d2f1443cf6))

## [1.4.75](https://gitlab.com/Veroxis/jeeves/compare/1.4.74...1.4.75) (2023-08-18)


### Bug Fixes

* **deps:** lock file maintenance ([c23a2c6](https://gitlab.com/Veroxis/jeeves/commit/c23a2c6f4cc15425c80f2378879d027807c1c549))

## [1.4.74](https://gitlab.com/Veroxis/jeeves/compare/1.4.73...1.4.74) (2023-08-17)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.22 ([8b6a4af](https://gitlab.com/Veroxis/jeeves/commit/8b6a4af65e53c329ddf4344ba11647145a828209))

## [1.4.73](https://gitlab.com/Veroxis/jeeves/compare/1.4.72...1.4.73) (2023-08-17)


### Bug Fixes

* **deps:** lock file maintenance ([26cfd34](https://gitlab.com/Veroxis/jeeves/commit/26cfd343a0716a799d7cf308b45817f0a6cd1aad))

## [1.4.72](https://gitlab.com/Veroxis/jeeves/compare/1.4.71...1.4.72) (2023-08-17)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.47 ([4c1e430](https://gitlab.com/Veroxis/jeeves/commit/4c1e43092f0e53839f508f084f0c6e8d80987a1c))

## [1.4.71](https://gitlab.com/Veroxis/jeeves/compare/1.4.70...1.4.71) (2023-08-17)


### Bug Fixes

* **deps:** update rust crate tokio to 1.32.0 ([a8134fa](https://gitlab.com/Veroxis/jeeves/commit/a8134faaa6fa7cde9d2b88657432cfb46244e983))

## [1.4.70](https://gitlab.com/Veroxis/jeeves/compare/1.4.69...1.4.70) (2023-08-16)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.105 ([76f70f5](https://gitlab.com/Veroxis/jeeves/commit/76f70f564e1a67b486a7d1d7d9efee55a9bb9624))

## [1.4.69](https://gitlab.com/Veroxis/jeeves/compare/1.4.68...1.4.69) (2023-08-15)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.46 ([e82a860](https://gitlab.com/Veroxis/jeeves/commit/e82a86086d31bd105038bbaa06f09e472ef7ee5f))

## [1.4.68](https://gitlab.com/Veroxis/jeeves/compare/1.4.67...1.4.68) (2023-08-15)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.45 ([f13f4ff](https://gitlab.com/Veroxis/jeeves/commit/f13f4ff87d0cf60eb35f1a2ec28b3814a38f85c8))

## [1.4.67](https://gitlab.com/Veroxis/jeeves/compare/1.4.66...1.4.67) (2023-08-15)


### Bug Fixes

* **deps:** update rust crate flate2 to 1.0.27 ([385f2b7](https://gitlab.com/Veroxis/jeeves/commit/385f2b742066a837f0ecd0c82b021b919f6028bd))

## [1.4.66](https://gitlab.com/Veroxis/jeeves/compare/1.4.65...1.4.66) (2023-08-15)


### Bug Fixes

* **deps:** lock file maintenance ([1bf398a](https://gitlab.com/Veroxis/jeeves/commit/1bf398a6c61eac66649de4b1abf34ee563e35ffb))

## [1.4.65](https://gitlab.com/Veroxis/jeeves/compare/1.4.64...1.4.65) (2023-08-13)


### Bug Fixes

* **deps:** lock file maintenance ([e4b5e80](https://gitlab.com/Veroxis/jeeves/commit/e4b5e80eb1e82203deb97d11b591b8fdbcc18589))

## [1.4.64](https://gitlab.com/Veroxis/jeeves/compare/1.4.63...1.4.64) (2023-08-12)


### Bug Fixes

* **deps:** update rust crate tokio to 1.31.0 ([30ea992](https://gitlab.com/Veroxis/jeeves/commit/30ea99250348b3d1b67a008962f5611d5059bc14))

## [1.4.63](https://gitlab.com/Veroxis/jeeves/compare/1.4.62...1.4.63) (2023-08-12)


### Bug Fixes

* **deps:** lock file maintenance ([dd5f45b](https://gitlab.com/Veroxis/jeeves/commit/dd5f45b98b194ba0d7f910f9d5363877c8c2ab51))

## [1.4.62](https://gitlab.com/Veroxis/jeeves/compare/1.4.61...1.4.62) (2023-08-12)


### Bug Fixes

* **deps:** update rust crate async-trait to 0.1.73 ([962614a](https://gitlab.com/Veroxis/jeeves/commit/962614a171d8789255518aadbe54574f14dcf193))

## [1.4.61](https://gitlab.com/Veroxis/jeeves/compare/1.4.60...1.4.61) (2023-08-11)


### Bug Fixes

* **deps:** lock file maintenance ([736db50](https://gitlab.com/Veroxis/jeeves/commit/736db500b701620321e4b900f6f41deb8315da53))

## [1.4.60](https://gitlab.com/Veroxis/jeeves/compare/1.4.59...1.4.60) (2023-08-10)


### Bug Fixes

* **deps:** lock file maintenance ([5f117ae](https://gitlab.com/Veroxis/jeeves/commit/5f117ae9547f3759ee7bc36767251cfc21b62144))

## [1.4.59](https://gitlab.com/Veroxis/jeeves/compare/1.4.58...1.4.59) (2023-08-09)


### Bug Fixes

* **deps:** lock file maintenance ([fdbcbe9](https://gitlab.com/Veroxis/jeeves/commit/fdbcbe9fdacaeb47eaf09362ee7bc418f54fe945))

## [1.4.58](https://gitlab.com/Veroxis/jeeves/compare/1.4.57...1.4.58) (2023-08-09)


### Bug Fixes

* **deps:** update rust crate tokio to 1.30.0 ([e9528b6](https://gitlab.com/Veroxis/jeeves/commit/e9528b66f3451d7ef2f38ee4bf04ea55cb9cace9))

## [1.4.57](https://gitlab.com/Veroxis/jeeves/compare/1.4.56...1.4.57) (2023-08-08)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.21 ([9447a5e](https://gitlab.com/Veroxis/jeeves/commit/9447a5ec2dcd5abae892d190537494d607f39b98))

## [1.4.56](https://gitlab.com/Veroxis/jeeves/compare/1.4.55...1.4.56) (2023-08-07)


### Bug Fixes

* **deps:** update rust crate tar to 0.4.40 ([a85d49e](https://gitlab.com/Veroxis/jeeves/commit/a85d49ed33ca5504c9538a87dab763401a5b27d5))

## [1.4.55](https://gitlab.com/Veroxis/jeeves/compare/1.4.54...1.4.55) (2023-08-07)


### Bug Fixes

* **deps:** lock file maintenance ([9cf91bf](https://gitlab.com/Veroxis/jeeves/commit/9cf91bf7e0814faab51960d9fdb16052f99afe08))

## [1.4.54](https://gitlab.com/Veroxis/jeeves/compare/1.4.53...1.4.54) (2023-08-07)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.183 ([bd77823](https://gitlab.com/Veroxis/jeeves/commit/bd77823d8e22bf5a76613ddac804f373058516f2))

## [1.4.53](https://gitlab.com/Veroxis/jeeves/compare/1.4.52...1.4.53) (2023-08-07)


### Bug Fixes

* **deps:** lock file maintenance ([d30b731](https://gitlab.com/Veroxis/jeeves/commit/d30b7319ea4838ef2f0446a4d891df715dc02db3))

## [1.4.52](https://gitlab.com/Veroxis/jeeves/compare/1.4.51...1.4.52) (2023-08-06)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.182 ([8a183b3](https://gitlab.com/Veroxis/jeeves/commit/8a183b37dfeb7d788b1f90e06d0b157be36fa10a))

## [1.4.51](https://gitlab.com/Veroxis/jeeves/compare/1.4.50...1.4.51) (2023-08-06)


### Bug Fixes

* **deps:** lock file maintenance ([3e5d470](https://gitlab.com/Veroxis/jeeves/commit/3e5d4703ae14c150906f52947a298e5ec8983fd3))

## [1.4.50](https://gitlab.com/Veroxis/jeeves/compare/1.4.49...1.4.50) (2023-08-05)


### Bug Fixes

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.71.1 ([0b6c34c](https://gitlab.com/Veroxis/jeeves/commit/0b6c34c0cded795761eea9f90bb2e7af9ad1d745))

## [1.4.49](https://gitlab.com/Veroxis/jeeves/compare/1.4.48...1.4.49) (2023-08-04)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.181 ([12219bc](https://gitlab.com/Veroxis/jeeves/commit/12219bc8296b706fc3463feaba6a5a7259276786))

## [1.4.48](https://gitlab.com/Veroxis/jeeves/compare/1.4.47...1.4.48) (2023-08-03)


### Bug Fixes

* **deps:** lock file maintenance ([bdf0bd5](https://gitlab.com/Veroxis/jeeves/commit/bdf0bd510e7792b27b06833d4cd5581ff53fabad))

## [1.4.47](https://gitlab.com/Veroxis/jeeves/compare/1.4.46...1.4.47) (2023-08-02)


### Bug Fixes

* **deps:** lock file maintenance ([ed7aa62](https://gitlab.com/Veroxis/jeeves/commit/ed7aa6291bfcd7af04e30c0d5c6c7058f582076d))

## [1.4.46](https://gitlab.com/Veroxis/jeeves/compare/1.4.45...1.4.46) (2023-08-01)


### Bug Fixes

* **deps:** lock file maintenance ([7cdab80](https://gitlab.com/Veroxis/jeeves/commit/7cdab8043a69f6476633375835008057f7d465cf))

## [1.4.45](https://gitlab.com/Veroxis/jeeves/compare/1.4.44...1.4.45) (2023-07-31)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.180 ([2a55a48](https://gitlab.com/Veroxis/jeeves/commit/2a55a48ebdfb380e81fefb536cdab39f0eeca491))

## [1.4.44](https://gitlab.com/Veroxis/jeeves/compare/1.4.43...1.4.44) (2023-07-31)


### Bug Fixes

* **deps:** lock file maintenance ([5a85e1c](https://gitlab.com/Veroxis/jeeves/commit/5a85e1cdf5a71c1f56acd244d668cdd09442ecae))

## [1.4.43](https://gitlab.com/Veroxis/jeeves/compare/1.4.42...1.4.43) (2023-07-31)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.179 ([99ba283](https://gitlab.com/Veroxis/jeeves/commit/99ba28380a2899557c983c4255dd2ff3d11474f6))

## [1.4.42](https://gitlab.com/Veroxis/jeeves/compare/1.4.41...1.4.42) (2023-07-30)


### Bug Fixes

* **deps:** lock file maintenance ([7f3c544](https://gitlab.com/Veroxis/jeeves/commit/7f3c544dcd0da6a699d02f5634742d89d17ea2a0))

## [1.4.41](https://gitlab.com/Veroxis/jeeves/compare/1.4.40...1.4.41) (2023-07-29)


### Bug Fixes

* **deps:** lock file maintenance ([38ad212](https://gitlab.com/Veroxis/jeeves/commit/38ad2126643c8b06a6ff0ccfeb4b2ef933c83fb0))

## [1.4.40](https://gitlab.com/Veroxis/jeeves/compare/1.4.39...1.4.40) (2023-07-29)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.178 ([12a30c5](https://gitlab.com/Veroxis/jeeves/commit/12a30c55b155584627daa92aab82ac64070dc7d0))

## [1.4.39](https://gitlab.com/Veroxis/jeeves/compare/1.4.38...1.4.39) (2023-07-27)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.177 ([f1138a2](https://gitlab.com/Veroxis/jeeves/commit/f1138a231a267c40cdf80223217a9798c235aca4))

## [1.4.38](https://gitlab.com/Veroxis/jeeves/compare/1.4.37...1.4.38) (2023-07-26)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.176 ([c3a262d](https://gitlab.com/Veroxis/jeeves/commit/c3a262d768590351ef42cfe4819826daca3702fc))

## [1.4.37](https://gitlab.com/Veroxis/jeeves/compare/1.4.36...1.4.37) (2023-07-26)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.104 ([eb9bd7d](https://gitlab.com/Veroxis/jeeves/commit/eb9bd7dcbd1b1e8e212f387df4c4f54544e90fcc))

## [1.4.36](https://gitlab.com/Veroxis/jeeves/compare/1.4.35...1.4.36) (2023-07-24)


### Bug Fixes

* **deps:** lock file maintenance ([be137a0](https://gitlab.com/Veroxis/jeeves/commit/be137a0168fb61774af786a960b82befd7540a7c))

## [1.4.35](https://gitlab.com/Veroxis/jeeves/compare/1.4.34...1.4.35) (2023-07-24)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.175 ([1b61ca0](https://gitlab.com/Veroxis/jeeves/commit/1b61ca03446de759716b22fb070962ad009422b2))

## [1.4.34](https://gitlab.com/Veroxis/jeeves/compare/1.4.33...1.4.34) (2023-07-22)


### Bug Fixes

* **deps:** lock file maintenance ([e2b9a70](https://gitlab.com/Veroxis/jeeves/commit/e2b9a70ecddae53378dd2d6a0a86d75fc1850598))

## [1.4.33](https://gitlab.com/Veroxis/jeeves/compare/1.4.32...1.4.33) (2023-07-21)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.19 ([4f0dc1c](https://gitlab.com/Veroxis/jeeves/commit/4f0dc1cd5c4465202e1b19b61179df0094d24132))

## [1.4.32](https://gitlab.com/Veroxis/jeeves/compare/1.4.31...1.4.32) (2023-07-21)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.18 ([535af9a](https://gitlab.com/Veroxis/jeeves/commit/535af9ae8c4dabd21b6b2167ead36a52d0af891c))

## [1.4.31](https://gitlab.com/Veroxis/jeeves/compare/1.4.30...1.4.31) (2023-07-21)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.174 ([9c64fe1](https://gitlab.com/Veroxis/jeeves/commit/9c64fe18665c828b20fcd237723d78fcc835c8bf))

## [1.4.30](https://gitlab.com/Veroxis/jeeves/compare/1.4.29...1.4.30) (2023-07-21)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.44 ([08c8d0f](https://gitlab.com/Veroxis/jeeves/commit/08c8d0f88fed78ca8766602f76f7020f33fc5d20))

## [1.4.29](https://gitlab.com/Veroxis/jeeves/compare/1.4.28...1.4.29) (2023-07-21)


### Bug Fixes

* **deps:** update rust crate async-trait to 0.1.72 ([684d4cf](https://gitlab.com/Veroxis/jeeves/commit/684d4cf8f6d610e77d28f9de9aacf9c6801ac302))

## [1.4.28](https://gitlab.com/Veroxis/jeeves/compare/1.4.27...1.4.28) (2023-07-21)


### Bug Fixes

* **deps:** lock file maintenance ([c77debb](https://gitlab.com/Veroxis/jeeves/commit/c77debb4367ea8f10e435835b59854cfc01a3767))

## [1.4.27](https://gitlab.com/Veroxis/jeeves/compare/1.4.26...1.4.27) (2023-07-20)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.173 ([9c89398](https://gitlab.com/Veroxis/jeeves/commit/9c8939848f44cf84a29f5912cf26f947175f6ff2))

## [1.4.26](https://gitlab.com/Veroxis/jeeves/compare/1.4.25...1.4.26) (2023-07-19)


### Bug Fixes

* **deps:** update rust crate serde to 1.0.172 ([e3a2e0f](https://gitlab.com/Veroxis/jeeves/commit/e3a2e0f269e136e40dd26da293b609dd3524c681))

## [1.4.25](https://gitlab.com/Veroxis/jeeves/compare/1.4.24...1.4.25) (2023-07-19)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.17 ([1ec24c1](https://gitlab.com/Veroxis/jeeves/commit/1ec24c1255c75b19997df129f4791b3e859fadd8))

## [1.4.24](https://gitlab.com/Veroxis/jeeves/compare/1.4.23...1.4.24) (2023-07-18)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.16 ([17d9009](https://gitlab.com/Veroxis/jeeves/commit/17d9009ed1e7666ad68aa3ef020d50ade5fd5928))

## [1.4.23](https://gitlab.com/Veroxis/jeeves/compare/1.4.22...1.4.23) (2023-07-18)


### Bug Fixes

* **deps:** lock file maintenance ([fd327b5](https://gitlab.com/Veroxis/jeeves/commit/fd327b5af786593dffc96153741e09c40583828f))

## [1.4.22](https://gitlab.com/Veroxis/jeeves/compare/1.4.21...1.4.22) (2023-07-18)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.15 ([cdddcb4](https://gitlab.com/Veroxis/jeeves/commit/cdddcb4f5fac40b56d2614615a62ca18010d22af))

## [1.4.21](https://gitlab.com/Veroxis/jeeves/compare/1.4.20...1.4.21) (2023-07-17)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.14 ([6e76c8b](https://gitlab.com/Veroxis/jeeves/commit/6e76c8b5975f36f91ee9c2376c90c9b374fde7fa))

## [1.4.20](https://gitlab.com/Veroxis/jeeves/compare/1.4.19...1.4.20) (2023-07-17)


### Bug Fixes

* **deps:** lock file maintenance ([d9565d8](https://gitlab.com/Veroxis/jeeves/commit/d9565d80ecb1ec2f7f06275632ecc2a0cc7484b8))

## [1.4.19](https://gitlab.com/Veroxis/jeeves/compare/1.4.18...1.4.19) (2023-07-16)


### Bug Fixes

* **deps:** lock file maintenance ([8d15c7f](https://gitlab.com/Veroxis/jeeves/commit/8d15c7fdc131303d2365a93717810fc12ed40a6e))

## [1.4.18](https://gitlab.com/Veroxis/jeeves/compare/1.4.17...1.4.18) (2023-07-16)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.103 ([3d6c3cc](https://gitlab.com/Veroxis/jeeves/commit/3d6c3cc204bcf434b5fe753020d597e92a586423))

## [1.4.17](https://gitlab.com/Veroxis/jeeves/compare/1.4.16...1.4.17) (2023-07-15)


### Bug Fixes

* **deps:** update rust crate semver to 1.0.18 ([6e3e21f](https://gitlab.com/Veroxis/jeeves/commit/6e3e21f4d1a8774347d04316a684805a7dc412c9))

## [1.4.16](https://gitlab.com/Veroxis/jeeves/compare/1.4.15...1.4.16) (2023-07-15)


### Bug Fixes

* **deps:** lock file maintenance ([6cca902](https://gitlab.com/Veroxis/jeeves/commit/6cca902a909570cd6681b3e8fef28d01e824d36e))

## [1.4.15](https://gitlab.com/Veroxis/jeeves/compare/1.4.14...1.4.15) (2023-07-14)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.12 ([ea64ffe](https://gitlab.com/Veroxis/jeeves/commit/ea64ffe601541419b0ddc272fe927cb02b0597d3))

## [1.4.14](https://gitlab.com/Veroxis/jeeves/compare/1.4.13...1.4.14) (2023-07-14)


### Bug Fixes

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker tag to v1.71.0 ([8b2dbcf](https://gitlab.com/Veroxis/jeeves/commit/8b2dbcf53dbb99607c3422d6fc513a176190460d))

## [1.4.13](https://gitlab.com/Veroxis/jeeves/compare/1.4.12...1.4.13) (2023-07-13)


### Bug Fixes

* **deps:** update rust crate tar to 0.4.39 ([f8e61e4](https://gitlab.com/Veroxis/jeeves/commit/f8e61e4fabdd5db20ddd3567095652ee53111e05))

## [1.4.12](https://gitlab.com/Veroxis/jeeves/compare/1.4.11...1.4.12) (2023-07-13)


### Bug Fixes

* **deps:** lock file maintenance ([0ec5ec5](https://gitlab.com/Veroxis/jeeves/commit/0ec5ec59f9f5f563f8d2df7d125265de2ef86a5c))

## [1.4.11](https://gitlab.com/Veroxis/jeeves/compare/1.4.10...1.4.11) (2023-07-12)


### Bug Fixes

* **deps:** lock file maintenance ([b2f4326](https://gitlab.com/Veroxis/jeeves/commit/b2f432698ff05695a87800645744a35badd3b666))

## [1.4.10](https://gitlab.com/Veroxis/jeeves/compare/1.4.9...1.4.10) (2023-07-12)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.102 ([5459e31](https://gitlab.com/Veroxis/jeeves/commit/5459e318c8a8e3de2d7c3de95f7f78f7976dbae3))

## [1.4.9](https://gitlab.com/Veroxis/jeeves/compare/1.4.8...1.4.9) (2023-07-12)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.101 ([1a6cc98](https://gitlab.com/Veroxis/jeeves/commit/1a6cc98c7fc572509007f8b3e563f0339d5e940a))

## [1.4.8](https://gitlab.com/Veroxis/jeeves/compare/1.4.7...1.4.8) (2023-07-11)


### Bug Fixes

* **deps:** lock file maintenance ([709b75b](https://gitlab.com/Veroxis/jeeves/commit/709b75bcad2c58841be403c6be89ab33378d248a))

## [1.4.7](https://gitlab.com/Veroxis/jeeves/compare/1.4.6...1.4.7) (2023-07-10)


### Bug Fixes

* **deps:** update rust crate thiserror to 1.0.43 ([ec164f1](https://gitlab.com/Veroxis/jeeves/commit/ec164f17e3f9a7c94b8d640bdb363a3d00aa001c))

## [1.4.6](https://gitlab.com/Veroxis/jeeves/compare/1.4.5...1.4.6) (2023-07-10)


### Bug Fixes

* **deps:** update rust crate clap to 4.3.11 ([537234b](https://gitlab.com/Veroxis/jeeves/commit/537234b768cb9aa67709d2d2f5c00340e21d37d3))

## [1.4.5](https://gitlab.com/Veroxis/jeeves/compare/1.4.4...1.4.5) (2023-07-10)


### Bug Fixes

* build with nightly toolchain ([d03d5f5](https://gitlab.com/Veroxis/jeeves/commit/d03d5f523ec6294246c43db8f0c698201ca6fffe))

## [1.4.4](https://gitlab.com/Veroxis/jeeves/compare/1.4.3...1.4.4) (2023-07-09)


### Bug Fixes

* build with stable toolchain ([142ae76](https://gitlab.com/Veroxis/jeeves/commit/142ae76c5872b3492d4cfad49448fa6f12b11a9d))

## [1.4.3](https://gitlab.com/Veroxis/jeeves/compare/1.4.2...1.4.3) (2023-06-08)


### Bug Fixes

* adapt self-updater to changes in gitlabs release api ([40757dc](https://gitlab.com/Veroxis/jeeves/commit/40757dcc8c52023e036f5f4d2c65060723140958))

## [1.4.2](https://gitlab.com/Veroxis/jeeves/compare/1.4.1...1.4.2) (2023-01-08)


### Bug Fixes

* update peer-dependencies ([34b8e51](https://gitlab.com/Veroxis/jeeves/commit/34b8e5195fba912e284932f6088f155ebb4c986c))

## [1.4.1](https://gitlab.com/Veroxis/jeeves/compare/1.4.0...1.4.1) (2022-12-17)


### Bug Fixes

* clippy lints ([8b02327](https://gitlab.com/Veroxis/jeeves/commit/8b02327a6763fa5925534464e3d06e63228d1469))

# [1.4.0](https://gitlab.com/Veroxis/jeeves/compare/1.3.0...1.4.0) (2022-10-25)


### Features

* **release:** use build-std to optimize release binary ([dc90e97](https://gitlab.com/Veroxis/jeeves/commit/dc90e97f931a3d1695dd756a752148658cb5b723))

# [1.3.0](https://gitlab.com/Veroxis/jeeves/compare/1.2.25...1.3.0) (2022-07-01)


### Features

* **runtime:** use `tokio` instead of `async-std` ([64d8d5b](https://gitlab.com/Veroxis/jeeves/commit/64d8d5bfa88744928825888cdb5efc9e8d8fce80))

## [1.2.25](https://gitlab.com/Veroxis/jeeves/compare/1.2.24...1.2.25) (2022-06-15)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.5 ([4e55a8c](https://gitlab.com/Veroxis/jeeves/commit/4e55a8cf97c83c815619e002c45cabdcf6893f69))

## [1.2.24](https://gitlab.com/Veroxis/jeeves/compare/1.2.23...1.2.24) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.4 ([4245a5f](https://gitlab.com/Veroxis/jeeves/commit/4245a5fd53ff583e66295d1e8e651b931b6c1b1b))

## [1.2.23](https://gitlab.com/Veroxis/jeeves/compare/1.2.22...1.2.23) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.3 ([0e9abc2](https://gitlab.com/Veroxis/jeeves/commit/0e9abc257309e51ca28d34295c6fb72f6ba4c6e5))

## [1.2.22](https://gitlab.com/Veroxis/jeeves/compare/1.2.21...1.2.22) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.2 ([14f57d3](https://gitlab.com/Veroxis/jeeves/commit/14f57d31d8a36f09b8d30bfa5e4991134c5d19a3))

## [1.2.21](https://gitlab.com/Veroxis/jeeves/compare/1.2.20...1.2.21) (2022-06-13)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.1 ([d9c2048](https://gitlab.com/Veroxis/jeeves/commit/d9c20485e49c2b0b79c3ea1ac4a1070c65e54dd5))
* use clap's new value_parser attribute ([38b0ef1](https://gitlab.com/Veroxis/jeeves/commit/38b0ef14989576341d0137568fac6801eb65e64b))

## [1.2.20](https://gitlab.com/Veroxis/jeeves/compare/1.2.19...1.2.20) (2022-06-10)


### Bug Fixes

* **deps:** update rust crate semver to 1.0.10 ([6fbc456](https://gitlab.com/Veroxis/jeeves/commit/6fbc4569d9e05a7f2cd80b71de98615c8df539ba))

## [1.2.19](https://gitlab.com/Veroxis/jeeves/compare/1.2.18...1.2.19) (2022-06-03)


### Bug Fixes

* **deps:** update rust crate async-trait to 0.1.56 ([87578c3](https://gitlab.com/Veroxis/jeeves/commit/87578c31594b27ff122b325d386d27f83ad62030))

## [1.2.18](https://gitlab.com/Veroxis/jeeves/compare/1.2.17...1.2.18) (2022-06-02)


### Bug Fixes

* **deps:** update rust crate async-trait to 0.1.54 ([fd706b2](https://gitlab.com/Veroxis/jeeves/commit/fd706b29b46eda712d6374765537dbfecd3b130e))

## [1.2.17](https://gitlab.com/Veroxis/jeeves/compare/1.2.16...1.2.17) (2022-05-29)


### Bug Fixes

* **ci:** implement semantic-release ([8ed88c9](https://gitlab.com/Veroxis/jeeves/commit/8ed88c9e45c46884f09f4904437bb3d4be266364))
