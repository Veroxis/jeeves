module.exports = {
    tagFormat: "${version}",
    branches: ['*'],
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/changelog",
        ['@semantic-release/git', {
            assets: ['CHANGELOG.md', "Cargo.toml", "Cargo.lock"],
        }],
        ['@semantic-release/gitlab', {
            assets: [{
                label: "jeeves",
                path: "target/x86_64-unknown-linux-musl/release/jeeves"
            }],
        }]
    ],
};
