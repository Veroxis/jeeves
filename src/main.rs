use anyhow::Result;

#[tokio::main]
async fn main() -> Result<()> {
    jeeves::run().await?;
    Ok(())
}
