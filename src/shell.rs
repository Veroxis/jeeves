use std::env;

use anyhow::anyhow;
use anyhow::Context;
use anyhow::Result;
use owo_colors::OwoColorize;
use tokio::fs;
use tokio::process::Command;

use crate::logger;

pub(crate) async fn command_exists(cmd: &str) -> bool {
    if let Ok(paths) = env::var("PATH") {
        for path in paths.split(':') {
            let p_str = format!("{path}/{cmd}");
            if fs::metadata(p_str).await.is_ok() {
                return true;
            }
        }
    }
    false
}

pub(crate) fn get_current_executable_path() -> Result<String> {
    let args = std::env::args().collect::<Vec<String>>();
    let arg0 = args
        .first()
        .expect("the first ARG must be the path to jeeves");
    if !arg0.contains('/') || arg0.starts_with('/') {
        Ok(arg0.to_owned())
    } else {
        let pwd = std::env::current_dir()
            .context("failed to get current directory")?
            .display()
            .to_string();
        let executable = format!("{pwd}/{arg0}");
        Ok(executable)
    }
}

pub(crate) async fn exec_command_verbose(command: &[&str]) -> Result<()> {
    let indicator = ">_".blue();
    let styled_command = command.join(" ").to_owned();
    let styled_command = styled_command.green();
    logger::info(&format!("{indicator} {styled_command}"));
    exec_command(command).await?;
    Ok(())
}

pub(crate) async fn exec_command(command: &[&str]) -> Result<()> {
    let executable = command.first().context("received empty command")?;
    assert!(!executable.is_empty(), "command can't be empty");
    let mut cmd = Command::new(executable);
    cmd.args(&command[1..]);
    match cmd.spawn()?.wait().await?.success() {
        true => Ok(()),
        false => Err(anyhow!("failed to execute command: {cmd:?}")),
    }
}
