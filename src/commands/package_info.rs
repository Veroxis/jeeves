use anyhow::Result;
use clap::Parser;

use crate::package_manager::brew::Brew;
use crate::package_manager::find_system_package_manager;
use crate::package_manager::flatpak::Flatpak;
use crate::package_manager::PackageManager;

#[derive(Debug, Parser)]
pub(crate) struct PackageInfoArgs {
    /// package name to query
    #[clap(name = "package")]
    pub package_name: String,

    /// Use flatpak instead of the system package manager
    #[clap(long, conflicts_with = "brew")]
    pub flatpak: bool,

    /// Use brew instead of the system package manager
    #[clap(long, conflicts_with = "flatpak")]
    pub brew: bool,
}

pub(crate) async fn package_info(args: PackageInfoArgs) -> Result<()> {
    if args.flatpak {
        let flatpak = Flatpak::new();
        return flatpak.info(Some(args.package_name)).await;
    }
    if args.brew {
        let brew = Brew::new();
        return brew.info(Some(args.package_name)).await;
    }
    let system_package_manager = find_system_package_manager().await?;
    system_package_manager.info(Some(args.package_name)).await
}
