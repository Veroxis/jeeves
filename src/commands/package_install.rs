use anyhow::Result;
use clap::Parser;

use crate::package_manager::brew::Brew;
use crate::package_manager::find_system_package_manager;
use crate::package_manager::flatpak::Flatpak;
use crate::package_manager::PackageManager;

#[derive(Debug, Parser)]
pub(crate) struct PackageInstallArgs {
    /// Use flatpak instead of the system package manager
    #[clap(long, conflicts_with = "brew")]
    pub flatpak: bool,

    /// Use brew instead of the system package manager
    #[clap(long, conflicts_with = "flatpak")]
    pub brew: bool,

    /// Insert default query
    #[clap(value_parser)]
    pub query: Option<Vec<String>>,
}

pub(crate) async fn package_install(args: PackageInstallArgs) -> Result<()> {
    if args.flatpak {
        let flatpak = Flatpak::new();
        return flatpak.install(args.query).await;
    }
    if args.brew {
        let brew = Brew::new();
        return brew.install(args.query).await;
    }
    let system_package_manager = find_system_package_manager().await?;
    system_package_manager.install(args.query).await
}
