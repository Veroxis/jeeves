use std::fs::File;
use std::io::Write;

use anyhow::anyhow;
use anyhow::Result;
use clap::crate_version;
use clap::Parser;
use owo_colors::OwoColorize;
use tokio::process::Command;

use crate::gitlab::GitlabReleaseApiResponse;
use crate::logger;
use crate::shell::command_exists;
use crate::shell::exec_command_verbose;

const GITLAB_PROJECT_ID: u32 = 29712393;

#[derive(Debug, Parser)]
pub(crate) struct UpdateSelfArgs {
    /// Install a specific release
    #[clap(short, long)]
    pub version: Option<String>,

    /// Do only list all available releases and exit
    #[clap(short, long)]
    pub list: bool,

    /// Install the newest development version
    #[clap(short, long)]
    pub dev: bool,
}

pub(crate) async fn update_self(args: UpdateSelfArgs) -> Result<()> {
    let gitlab_release_api_response = GitlabReleaseApiResponse::from(GITLAB_PROJECT_ID).await?;
    if args.list {
        let releases = gitlab_release_api_response.get_all_releases();
        println!("{releases:#?}");
        return Ok(());
    }
    let newest_version = gitlab_release_api_response
        .get_latest_version()?
        .to_string();
    logger::info(&format!("Current version: {}", crate_version!().yellow()));
    let url = match (args.dev, args.version) {
        (true, _) => "https://veroxis.gitlab.io/jeeves/release/jeeves".to_string(),
        (false, Some(version)) => gitlab_release_api_response.get_url_for_version(&version)?,
        (false, None) => gitlab_release_api_response.get_url_for_version(&newest_version)?,
    };
    let tmp_save_path = get_tmp_save_path().await?;
    let highlighted_url = url.yellow();
    logger::info(&format!("Downloading: {highlighted_url}"));
    let file_data = fetch_file_data(&url).await?;
    let kibibytes = file_data.len() / 1024;
    let size = format!("{kibibytes} KiB");
    let size_highlighted = size.yellow();
    logger::info(&format!("Download finished. Size: {size_highlighted}"));
    let save_path_highlighted = tmp_save_path.yellow();
    logger::info(&format!("Saving to path: {save_path_highlighted}"));
    save_bytes_to_file(&tmp_save_path, &file_data).await?;
    make_executable(&tmp_save_path).await?;
    sudo_move_file(&tmp_save_path, "/usr/local/bin/jeeves").await?;
    test_jeeves_works().await?;
    Ok(())
}

async fn fetch_file_data(url: &str) -> Result<Vec<u8>> {
    Ok(minreq::get(url).send()?.as_bytes().to_vec())
}

async fn get_tmp_save_path() -> Result<String> {
    let mut jeeves_tmp_path: String = match dirs::cache_dir() {
        Some(path) => match path.to_str() {
            Some(path) => {
                let mut path = path.to_owned();
                path.push('/');
                path
            }
            None => "/tmp/".into(),
        },
        None => "/tmp/".into(),
    };
    Command::new("mkdir")
        .args(["-p", &jeeves_tmp_path])
        .output()
        .await?;
    jeeves_tmp_path.push_str("jeeves");
    Ok(jeeves_tmp_path)
}

async fn save_bytes_to_file(path: &str, data: &[u8]) -> Result<()> {
    let mut file = File::create(path)?;
    file.write_all(data)?;
    Ok(())
}

async fn make_executable(path: &str) -> Result<()> {
    exec_command_verbose(&["chmod", "+x", path]).await?;
    Ok(())
}

async fn sudo_move_file(src: &str, dest: &str) -> Result<()> {
    exec_command_verbose(&["sudo", "mv", src, dest]).await?;
    Ok(())
}

async fn test_jeeves_works() -> Result<()> {
    match command_exists("jeeves").await {
        true => {
            exec_command_verbose(&["jeeves", "--version"]).await?;
            Ok(())
        }
        false => {
            let jeeves = "jeeves".yellow();
            let install_path = "/usr/local/bin/".yellow();
            Err(anyhow!(
                "Can't find {} in PATH. Is {} part of your PATH?",
                jeeves,
                install_path
            ))
        }
    }
}
