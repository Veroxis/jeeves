use std::process::Command;
use std::process::Stdio;

use anyhow::Result;
use clap::Parser;
use owo_colors::OwoColorize;

use crate::logger;
use crate::package_manager::brew::Brew;
use crate::package_manager::find_system_package_manager;
use crate::package_manager::flatpak::Flatpak;
use crate::package_manager::PackageManager;
use crate::shell::command_exists;
use crate::shell::exec_command_verbose;

#[derive(Debug, Parser)]
pub(crate) struct SystemUpdateArgs {
    /// Enable creating a snapshot before updating using `timeshift`
    #[clap(long)]
    pub enable_snapshot: bool,

    /// Skip the rust updates through `rustup` and `cargo-install-update`
    #[clap(long)]
    pub skip_rust: bool,

    /// Skip the system update through `yay`
    #[clap(long)]
    pub skip_system: bool,

    /// Skip the flatpak updates
    #[clap(long)]
    pub skip_flatpak: bool,

    /// Skip the brew updates
    #[clap(long)]
    pub skip_brew: bool,
}

pub(crate) async fn update_system(args: SystemUpdateArgs) -> Result<()> {
    if args.enable_snapshot {
        if command_exists("timeshift").await {
            exec_command_verbose(&[
                "sudo",
                "timeshift",
                "--create",
                "--comments",
                "update_system",
            ])
            .await?;
            while get_amount_existing_timeshift_snapshots()? > 5 {
                exec_command_verbose(&["sudo", "timeshift", "--list"]).await?;
                delete_oldest_snapshot().await?;
            }
        } else {
            logger::warn(&format!(
                "Snapshots are disabled since '{}' is not installed.",
                "timeshift".red()
            ));
        }
    }
    if !args.skip_system {
        match find_system_package_manager().await {
            Ok(system_package_manager) => system_package_manager.update().await?,
            Err(err) => logger::warn(&format!("{err}")),
        };
    }
    if !args.skip_flatpak && command_exists("flatpak").await {
        let flatpak = Flatpak::new();
        flatpak.update().await?;
    }
    if !args.skip_brew && command_exists("brew").await {
        let brew = Brew::new();
        brew.update().await?;
    }
    if !args.skip_rust && command_exists("rustup").await {
        exec_command_verbose(&["rustup", "update"]).await?;
        if command_exists("cargo-install-update").await {
            exec_command_verbose(&["cargo-install-update", "install-update", "-a"]).await?;
        } else {
            logger::info(&format!(
                "{} is installed but not {}.",
                "rustup".yellow(),
                "cargo-install-update".yellow()
            ));
            logger::info(&format!(
                "You can install it using {}",
                "cargo install cargo-update".cyan()
            ));
        }
    }
    Ok(())
}

fn get_amount_existing_timeshift_snapshots() -> Result<u64> {
    let cmd = Command::new("sudo")
        .args(["timeshift", "--list"])
        .stdin(Stdio::inherit())
        .output()?;
    let output = std::str::from_utf8(&cmd.stdout)?;
    let snapshots = output
        .split('\n')
        .filter(|line| line.contains("update_system"))
        .count() as u64;
    logger::info(&format!("Snapshots: {}", snapshots.yellow()));
    Ok(snapshots)
}

async fn delete_oldest_snapshot() -> Result<()> {
    let cmd = Command::new("sudo")
        .args(["timeshift", "--list"])
        .stdin(Stdio::inherit())
        .output()?;
    let output = std::str::from_utf8(&cmd.stdout)?;
    if let Some(line) = output
        .split('\n')
        .filter(|line| line.contains("update_system"))
        .find(|_| true)
    {
        let mut nth_word = 0;
        for str in line.split(' ') {
            if !str.is_empty() {
                nth_word += 1;
            }
            if nth_word == 3 {
                let timeshift_snapshot_id = str;
                logger::info(&format!(
                    "Deleting Snapshot: {}",
                    timeshift_snapshot_id.yellow()
                ));
                exec_command_verbose(&[
                    "sudo",
                    "timeshift",
                    "--delete",
                    "--snapshot",
                    timeshift_snapshot_id,
                ])
                .await?;
                break;
            }
        }
    }
    Ok(())
}
