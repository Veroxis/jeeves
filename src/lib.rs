pub mod commands;
pub mod gitlab;
pub mod logger;
pub mod package_manager;
pub mod shell;
pub mod ui;

use anyhow::anyhow;
use anyhow::Result;
use clap::Parser;
use commands::package_delete::package_delete;
use commands::package_delete::PackageDeleteArgs;
use commands::package_info::package_info;
use commands::package_info::PackageInfoArgs;
use commands::package_install::package_install;
use commands::package_install::PackageInstallArgs;
use commands::self_update::update_self;
use commands::self_update::UpdateSelfArgs;
use commands::system_update::update_system;
use commands::system_update::SystemUpdateArgs;
use shell::command_exists;

const ALIASES_UPDATE_SYSTEM: &[&str; 7] = &[
    "system-update",
    "su",
    "update-system",
    "system_update",
    "us",
    "update_system",
    "u",
];

const ALIASES_PACKAGE_INSTALL: &[&str; 14] = &[
    "i",
    "package-install",
    "package_install",
    "pi",
    "package-add",
    "package_add",
    "pa",
    "install-package",
    "install_package",
    "add-package",
    "add_package",
    "browse_packagelist",
    "add",
    "a",
];

const ALIASES_PACKAGE_DELETE: &[&str; 15] = &[
    "package-delete",
    "d",
    "r",
    "rm",
    "remove",
    "pd",
    "package_delete",
    "pr",
    "package-remove",
    "package_remove",
    "remove-package",
    "remove_package",
    "delete_package",
    "uninstall",
    "deinstall",
];

const ALIASES_PACKAGE_INFO: &[&str; 4] = &["package-info", "package_info", "describe", "inspect"];

const ALIASES_SELF_UPDATE: &[&str; 3] = &["update-self", "self_update", "update_self"];

#[derive(Parser, Debug)]
#[clap(name = "commands", about)]
pub(crate) enum Commands {
    /// system update macro which uses all package managers it can detect
    #[clap(name = "update", aliases = ALIASES_UPDATE_SYSTEM)]
    SystemUpdate(SystemUpdateArgs),

    /// interactive package installation
    #[clap(name = "install", aliases = ALIASES_PACKAGE_INSTALL)]
    PackageInstall(PackageInstallArgs),

    /// interactive deletion of installed packages
    #[clap(name = "delete", aliases = ALIASES_PACKAGE_DELETE)]
    PackageDelete(PackageDeleteArgs),

    /// print information about the given package
    #[clap(name = "info", aliases = ALIASES_PACKAGE_INFO)]
    PackageInfo(PackageInfoArgs),

    /// self-updater
    #[clap(name = "self-update", aliases = ALIASES_SELF_UPDATE)]
    UpdateSelf(UpdateSelfArgs),
}

#[derive(Debug, Parser)]
#[clap(name = "jeeves", author, version, about, long_about = None)]
pub(crate) struct JeevesArguments {
    #[clap(subcommand)]
    pub command: Commands,
}

pub(crate) async fn init_logging() -> Result<()> {
    #[cfg(debug_assertions)]
    {
        if std::env::var("RUST_LIB_BACKTRACE").is_err() {
            std::env::set_var("RUST_LIB_BACKTRACE", "1")
        }
        if std::env::var("RUST_BACKTRACE").is_err() {
            std::env::set_var("RUST_BACKTRACE", "full")
        }
    }

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info")
    }

    Ok(())
}

pub(crate) async fn check_runtime_requirements() -> Result<()> {
    if !command_exists("sudo").await {
        return Err(anyhow!("`jeeves` requires `sudo` to be installed"));
    }
    Ok(())
}

pub(crate) async fn run_jeeves() -> Result<()> {
    let args = JeevesArguments::parse();
    match args.command {
        Commands::UpdateSelf(args) => update_self(args).await,
        Commands::SystemUpdate(args) => update_system(args).await,
        Commands::PackageInstall(args) => package_install(args).await,
        Commands::PackageDelete(args) => package_delete(args).await,
        Commands::PackageInfo(args) => package_info(args).await,
    }
}

pub async fn run() -> Result<()> {
    init_logging().await?;
    check_runtime_requirements().await?;
    run_jeeves().await?;
    Ok(())
}
