use std::fmt::Display;

use owo_colors::OwoColorize;

enum Severity {
    Debug,
    Info,
    Warn,
}

impl Severity {
    pub fn as_color_string(&self) -> String {
        match self {
            Severity::Debug => format!("[{}]", "DEBUG".blue()),
            Severity::Info => format!("[{}]", "INFO".green()),
            Severity::Warn => format!("[{}]", "WARN".red()),
        }
    }
}

impl Display for Severity {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.as_color_string())
    }
}

pub fn debug(msg: &str) {
    print(Severity::Debug, msg);
}

pub fn info(msg: &str) {
    print(Severity::Info, msg);
}

pub fn warn(msg: &str) {
    print(Severity::Warn, msg);
}

fn print(severity: Severity, msg: &str) {
    println!("{severity} {msg}");
}
