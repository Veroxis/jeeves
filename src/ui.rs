use std::io::Cursor;

use skim::prelude::SkimItemReader;
use skim::prelude::SkimOptionsBuilder;
use skim::FuzzyAlgorithm;
use skim::Skim;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum UiError {
    #[error("SkimErrorMessage: {0}")]
    SkimErrorMessage(String),

    #[error("selection list is empty")]
    EmptySelectionList,

    #[error("no package was selected")]
    NoPackageSelected,
}

pub(crate) fn show_select_package_prompt(
    packages: &[String],
    preview_command: Option<String>,
) -> Result<String, UiError> {
    let mut options = SkimOptionsBuilder::default();
    options.multi(false);
    options.algorithm(FuzzyAlgorithm::Clangd);
    options.reverse(false);
    options.color(Some("16".to_string()));
    if let Some(cmd) = preview_command {
        options.preview(Some(cmd));
        options.preview_window("right:60%".to_string());
    }
    let options = options
        .build()
        .map_err(|msg| UiError::SkimErrorMessage(msg.to_string()))?;

    let item_reader = SkimItemReader::default();
    let items = item_reader.of_bufread(Cursor::new(packages.join("\n")));

    let selected_items = Skim::run_with(&options, Some(items))
        .map(|out| match out.final_event {
            skim::prelude::Event::EvActCancel => {
                std::process::exit(0);
            }
            skim::prelude::Event::EvActAbort => {
                std::process::exit(0);
            }
            _ => out.selected_items,
        })
        .ok_or(UiError::EmptySelectionList)?;

    let selected = selected_items
        .iter()
        .find(|_| true)
        .ok_or(UiError::NoPackageSelected)?
        .output();

    Ok(selected.into())
}
