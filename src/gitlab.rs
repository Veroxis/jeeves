use std::collections::HashMap;

use chrono::prelude::*;
use semver::Version;
use serde::Deserialize;
use serde::Serialize;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum ReleaseApiError {
    #[error("can't parse Version: `{0}`")]
    SemverError(String),

    #[error("minreq::Error: `{0}`")]
    MinReqError(#[from] minreq::Error),

    #[error("SerdeJsonError: `{0}`")]
    SerdeJsonError(#[from] serde_json::Error),

    #[error("can't find any semver compatible version")]
    CantFindNewestVersion,

    #[error("can't find url for version: `{0}`")]
    CantFindUrlForVersion(String),

    #[error("Can't parse Version: `{0}`")]
    Other(String),
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct GitlabAuthor {
    pub id: u128,
    pub name: String,
    pub username: String,
    pub state: String,
    pub avatar_url: String,
    pub web_url: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct GitlabCommit {
    pub id: String,
    pub short_id: String,
    pub created_at: String,
    pub parent_ids: Vec<String>,
    pub title: String,
    pub message: String,
    pub author_name: String,
    pub author_email: String,
    pub authored_date: DateTime<Utc>,
    pub committer_name: String,
    pub committer_email: String,
    pub committed_date: DateTime<Utc>,
    // trailers: {},
    pub web_url: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct GitlabReleaseAssets {
    pub count: u128,
    pub sources: Vec<GitlabReleaseAsset>,
    pub links: Vec<GitlabReleaseAssetLink>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct GitlabReleaseAsset {
    pub format: String,
    pub url: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct GitlabReleaseAssetLink {
    pub id: u128,
    pub name: String,
    pub url: String,
    pub direct_asset_url: String,
    pub link_type: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct GitlabReleaseEvidence {
    pub sha: String,
    pub filepath: String,
    pub collected_at: DateTime<Utc>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct GitlabReleaseInfo {
    pub name: String,
    pub tag_name: String,
    pub description: Option<String>,
    pub created_at: DateTime<Utc>,
    pub released_at: DateTime<Utc>,
    pub author: GitlabAuthor,
    pub commit: GitlabCommit,
    pub upcoming_release: bool,
    pub commit_path: String,
    pub tag_path: String,
    pub assets: GitlabReleaseAssets,
    pub evidences: Vec<GitlabReleaseEvidence>,
    #[serde(rename = "_links")]
    pub links: HashMap<String, String>,
}

impl GitlabReleaseInfo {
    pub(crate) fn get_version_as_semver(&self) -> Result<Version, ReleaseApiError> {
        Version::parse(&self.tag_name).map_err(|e| ReleaseApiError::SemverError(e.to_string()))
    }

    pub(crate) fn get_asset_download_url(&self) -> Result<String, ReleaseApiError> {
        match self.assets.links.first() {
            Some(asset_link) => Ok(asset_link.direct_asset_url.clone()),
            None => Err(ReleaseApiError::Other(format!(
                "can't find asset link in version {}",
                self.tag_name
            ))),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct GitlabReleaseApiResponse(Vec<GitlabReleaseInfo>);

impl GitlabReleaseApiResponse {
    pub(crate) async fn from(project_id: u32) -> Result<Self, ReleaseApiError> {
        let response = minreq::get(format!(
            "https://gitlab.com/api/v4/projects/{project_id}/releases"
        ))
        .send()?
        .as_str()?
        .to_owned();
        Ok(serde_json::from_str(&response)?)
    }

    pub(crate) fn get_all_releases(&self) -> Vec<String> {
        let mut releases = vec![];
        for release in self.0.iter() {
            if let Ok(version) = release.get_version_as_semver() {
                releases.push(version.to_string());
            }
        }
        releases
    }

    pub(crate) fn get_url_for_version(&self, version: &str) -> Result<String, ReleaseApiError> {
        for release in self.0.iter() {
            if release.tag_name == version {
                return release.get_asset_download_url();
            }
        }
        Err(ReleaseApiError::CantFindUrlForVersion(version.into()))
    }

    pub(crate) fn get_latest_version(&self) -> Result<Version, ReleaseApiError> {
        let mut highest_version: Option<Version> = None;
        for v in self.0.iter() {
            if let Ok(version) = v.get_version_as_semver() {
                highest_version = match highest_version {
                    Some(v) => match version.gt(&v) {
                        true => Some(version),
                        false => Some(v),
                    },
                    None => Some(version),
                }
            }
        }
        match highest_version {
            Some(version) => Ok(version),
            None => Err(ReleaseApiError::CantFindNewestVersion),
        }
    }
}
