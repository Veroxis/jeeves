use std::process::Stdio;

use anyhow::Context;
use anyhow::Result;
use async_trait::async_trait;
use tokio::process::Command;

use super::PackageManager;
use crate::shell::exec_command_verbose;

#[derive(Debug, Default)]
pub(crate) struct Dnf {}

impl Dnf {
    pub(crate) fn new() -> Self {
        Self {}
    }

    async fn get_package_list(&self) -> Result<Vec<String>> {
        let cmd = Command::new("dnf")
            .args(["list", "available", "-q"])
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let mut buff = output
            .split('\n')
            .skip(2)
            .filter_map(|line| match line.is_empty() {
                true => None,
                false => line.split_whitespace().next().map(|elem| elem.to_string()),
            })
            .collect::<Vec<String>>();
        buff.sort();
        Ok(buff)
    }

    async fn get_installed_packages(&self) -> Result<Vec<String>> {
        let cmd = Command::new("dnf")
            .args(["list", "installed", "-q"])
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let mut buff = output
            .split('\n')
            .skip(1)
            .filter_map(|line| match line.is_empty() {
                true => None,
                false => line.split_whitespace().next().map(|elem| elem.to_string()),
            })
            .collect::<Vec<String>>();
        buff.sort();
        Ok(buff)
    }
}

#[async_trait]
impl PackageManager for Dnf {
    async fn install(&self, query: Option<Vec<String>>) -> Result<()> {
        let _ = exec_command_verbose(&["sudo", "dnf", "check-update", "--refresh"]).await;
        if let Some(packages) = query {
            for package in packages.iter() {
                exec_command_verbose(&["sudo", "dnf", "install", package])
                    .await
                    .context(format!("failed to install package {package}"))?;
            }
            return Ok(());
        }
        let packages = self.get_package_list().await?;
        if !packages.is_empty() {
            let selected_package = self.get_selected_package(&packages)?;
            exec_command_verbose(&["sudo", "dnf", "install", selected_package.as_str()]).await?;
        }
        Ok(())
    }

    async fn delete(&self, query: Option<Vec<String>>) -> Result<()> {
        if let Some(packages) = query {
            for package in packages.iter() {
                exec_command_verbose(&["sudo", "dnf", "remove", package])
                    .await
                    .context(format!("failed to remove package {package}"))?;
            }
            return Ok(());
        }
        let packages = self.get_installed_packages().await?;
        if !packages.is_empty() {
            let selected_package = self.get_selected_package(&packages)?;
            exec_command_verbose(&["sudo", "dnf", "remove", selected_package.as_str()]).await?;
        }
        Ok(())
    }

    async fn update(&self) -> Result<()> {
        exec_command_verbose(&["sudo", "dnf", "distro-sync", "--refresh", "--assumeyes"]).await?;
        Ok(())
    }

    async fn info(&self, query: Option<String>) -> Result<()> {
        if let Some(package) = query {
            let _ = exec_command_verbose(&["dnf", "info", package.as_str()]).await;
        }
        Ok(())
    }
}
