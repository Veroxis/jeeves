use std::process::Stdio;

use anyhow::Context;
use anyhow::Result;
use async_trait::async_trait;
use tokio::process::Command;

use super::PackageManager;
use crate::shell::exec_command_verbose;

#[derive(Debug, Default)]
pub(crate) struct Apt {}

impl Apt {
    pub(crate) fn new() -> Self {
        Self {}
    }

    async fn get_package_list(&self) -> Result<Vec<String>> {
        let cmd = Command::new("apt-cache")
            .args(["pkgnames"])
            .stderr(Stdio::inherit())
            .stdin(Stdio::piped())
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let mut buff = output
            .split('\n')
            .filter_map(|line| match line.is_empty() {
                true => None,
                false => Some(line.to_string()),
            })
            .collect::<Vec<String>>();
        buff.sort();
        Ok(buff)
    }

    async fn get_installed_packages(&self) -> Result<Vec<String>> {
        let cmd = Command::new("dpkg")
            .args(["--get-selections"])
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let mut buff = output
            .split('\n')
            .filter_map(|line| match line.is_empty() || line.contains("deinstall") {
                true => None,
                false => line.split_whitespace().next().map(|elem| elem.to_string()),
            })
            .collect::<Vec<String>>();
        buff.sort();
        Ok(buff)
    }
}

#[async_trait]
impl PackageManager for Apt {
    async fn install(&self, query: Option<Vec<String>>) -> Result<()> {
        exec_command_verbose(&["sudo", "apt-get", "update"]).await?;
        if let Some(packages) = query {
            for package in packages.iter() {
                exec_command_verbose(&["sudo", "apt-get", "install", package])
                    .await
                    .context(format!("failed to install package {package}"))?;
            }
            return Ok(());
        }
        let packages = self.get_package_list().await?;
        if !packages.is_empty() {
            let selected_package = self.get_selected_package(&packages)?;
            exec_command_verbose(&["sudo", "apt-get", "install", selected_package.as_str()])
                .await?;
        }
        Ok(())
    }

    async fn delete(&self, query: Option<Vec<String>>) -> Result<()> {
        if let Some(packages) = query {
            for package in packages.iter() {
                exec_command_verbose(&[
                    "sudo",
                    "apt-get",
                    "remove",
                    "--autoremove",
                    package.as_str(),
                ])
                .await
                .context(format!("failed to remove package {package}"))?;
            }
            return Ok(());
        }
        let packages = self.get_installed_packages().await?;
        if !packages.is_empty() {
            let selected_package = self.get_selected_package(&packages)?;
            exec_command_verbose(&[
                "sudo",
                "apt-get",
                "remove",
                "--autoremove",
                selected_package.as_str(),
            ])
            .await?;
        }
        Ok(())
    }

    async fn update(&self) -> Result<()> {
        exec_command_verbose(&["sudo", "apt-get", "update"]).await?;
        exec_command_verbose(&["sudo", "apt-get", "full-upgrade", "-y"]).await?;
        Ok(())
    }

    async fn info(&self, query: Option<String>) -> Result<()> {
        if let Some(package) = query {
            let _ = exec_command_verbose(&["apt-cache", "show", package.as_str()]).await;
        }
        Ok(())
    }
}
