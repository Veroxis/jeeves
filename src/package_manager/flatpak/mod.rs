use std::process::Stdio;

use anyhow::anyhow;
use anyhow::Context;
use anyhow::Result;
use async_trait::async_trait;
use tokio::process::Command;

use super::PackageManager;
use crate::shell::exec_command_verbose;
use crate::shell::get_current_executable_path;

#[derive(Debug, Default)]
pub(crate) struct Flatpak {}

impl Flatpak {
    pub(crate) fn new() -> Self {
        Self {}
    }

    async fn get_package_list(&self) -> Result<Vec<String>> {
        let cmd = Command::new("flatpak")
            .args(["remote-ls", "--user", "--columns=origin,application"])
            .stderr(Stdio::inherit())
            .stdin(Stdio::piped())
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let mut buff = output
            .split('\n')
            .filter_map(|line| match line.is_empty() {
                true => None,
                false => Some(line.to_string().replace('\t', " ")),
            })
            .collect::<Vec<String>>();
        buff.sort();
        Ok(buff)
    }

    async fn get_repositories(&self) -> Result<Vec<String>> {
        let cmd = Command::new("flatpak")
            .args(["remotes", "--user", "--columns=name"])
            .stderr(Stdio::inherit())
            .stdin(Stdio::piped())
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let buff = output
            .split('\n')
            .filter_map(|line| match line.is_empty() {
                true => None,
                false => line.split_whitespace().next().map(|elem| elem.to_string()),
            })
            .collect::<Vec<String>>();
        Ok(buff)
    }

    async fn get_installed_packages(&self) -> Result<Vec<String>> {
        let cmd = Command::new("flatpak")
            .args(["list", "--user", "--columns=origin,application"])
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let mut buff = output
            .split('\n')
            .filter_map(|line| match line.is_empty() {
                true => None,
                false => Some(line.replace('\t', " ")),
            })
            .collect::<Vec<String>>();
        buff.sort();
        Ok(buff)
    }
}

#[async_trait]
impl PackageManager for Flatpak {
    fn get_preview_command(&self) -> Result<String> {
        let jeeves = get_current_executable_path()?;
        Ok(format!("{jeeves} info --flatpak {{2}}"))
    }

    async fn install(&self, query: Option<Vec<String>>) -> Result<()> {
        exec_command_verbose(&["flatpak", "update"]).await?;
        if let Some(packages) = query {
            for package in packages.iter() {
                exec_command_verbose(&["flatpak", "install", "--user", package])
                    .await
                    .context(format!("failed to install package {package}"))?;
            }
            return Ok(());
        }
        let packages = self.get_package_list().await?;
        if !packages.is_empty() {
            let selected_package = self.get_selected_package(&packages)?;
            let package = selected_package
                .split_whitespace()
                .nth(1)
                .ok_or_else(|| anyhow!("selection list is empty"))?
                .to_string();
            exec_command_verbose(&["flatpak", "install", "--user", package.as_str()]).await?;
        }
        Ok(())
    }

    async fn delete(&self, query: Option<Vec<String>>) -> Result<()> {
        if let Some(packages) = query {
            for package in packages.iter() {
                exec_command_verbose(&["flatpak", "uninstall", "--user", package])
                    .await
                    .context(format!("failed to remove package {package}"))?;
            }
            return Ok(());
        }
        let packages = self.get_installed_packages().await?;
        if !packages.is_empty() {
            let selected_package = self.get_selected_package(&packages)?;
            let package = selected_package
                .split_whitespace()
                .nth(1)
                .ok_or_else(|| anyhow!("selection list is empty"))?
                .to_string();
            exec_command_verbose(&["flatpak", "uninstall", "--user", package.as_str()]).await?;
        }
        Ok(())
    }

    async fn update(&self) -> Result<()> {
        exec_command_verbose(&["flatpak", "update", "--noninteractive", "--user"]).await?;
        Ok(())
    }

    async fn info(&self, query: Option<String>) -> Result<()> {
        if let Some(package) = query {
            let _ = exec_command_verbose(&["flatpak", "info", "--user", package.as_str()]).await;
            let repositories = self.get_repositories().await?;
            for repository in repositories.iter() {
                let _ = exec_command_verbose(&[
                    "flatpak",
                    "remote-info",
                    "--user",
                    repository,
                    package.as_str(),
                ])
                .await;
            }
        }
        Ok(())
    }
}
