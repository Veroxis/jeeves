pub mod apk;
pub mod apt;
pub mod brew;
pub mod dnf;
pub mod flatpak;
pub mod pacman;

use anyhow::anyhow;
use anyhow::Result;
use async_trait::async_trait;

use self::apt::Apt;
use self::dnf::Dnf;
use self::pacman::Pacman;
use crate::package_manager::apk::Apk;
use crate::shell::command_exists;
use crate::shell::get_current_executable_path;
use crate::ui::show_select_package_prompt;

pub(crate) async fn find_system_package_manager() -> Result<Box<dyn PackageManager>> {
    if command_exists("pacman").await {
        return Ok(Box::new(Pacman::new().await));
    }
    if command_exists("apk").await {
        return Ok(Box::new(Apk::new()));
    }
    if command_exists("apt-get").await {
        return Ok(Box::new(Apt::new()));
    }
    if command_exists("dnf").await {
        return Ok(Box::new(Dnf::new()));
    }
    Err(anyhow!(
        "Can't detect any system package manager. It's either not implemented or doesn't exist."
    ))
}

/// Cross-Platform definition of required package manager operations for this appliaction
#[async_trait]
pub trait PackageManager {
    /// Starts a package selection and installation routine
    async fn install(&self, query: Option<Vec<String>>) -> Result<()>;
    /// Starts a package selection and deletion routine
    async fn delete(&self, query: Option<Vec<String>>) -> Result<()>;
    /// Prints details about the given package
    async fn info(&self, query: Option<String>) -> Result<()>;
    /// Starts a system update
    async fn update(&self) -> Result<()>;
    /// Returns the preview command which is needed for skim
    fn get_preview_command(&self) -> Result<String> {
        Ok(format!(
            "{jeeves} info {{1}}",
            jeeves = get_current_executable_path()?
        ))
    }
    /// Open skim using the packages for selection and the preview command to show details about them
    fn get_selected_package(&self, packages: &[String]) -> Result<String> {
        Ok(show_select_package_prompt(
            packages,
            Some(self.get_preview_command()?),
        )?)
    }
}
