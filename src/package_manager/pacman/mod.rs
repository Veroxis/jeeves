mod pacman_wrapper;

use std::io::Read;
use std::process::Stdio;

use anyhow::Context;
use anyhow::Result;
use async_trait::async_trait;
use flate2::read::GzDecoder;
use futures::join;
use pacman_wrapper::PacmanWrapper;
use tokio::process::Command;

use super::PackageManager;
use crate::shell::exec_command_verbose;

#[derive(Debug, Default)]
pub(crate) struct Pacman {
    pacman_wrapper: Option<PacmanWrapper>,
}

impl Pacman {
    pub(crate) async fn new() -> Self {
        Self {
            pacman_wrapper: PacmanWrapper::detect_existing().await,
        }
    }

    async fn fetch_aur_package_list(&self) -> Result<Vec<String>> {
        let response = minreq::get("https://aur.archlinux.org/packages.gz")
            .send()?
            .as_bytes()
            .to_owned();
        let mut response_string = String::new();
        let mut decoder = GzDecoder::new(response.as_slice());
        decoder.read_to_string(&mut response_string)?;
        let mut buff = response_string
            .split('\n')
            .filter_map(|line| match line.starts_with('#') || line.is_empty() {
                true => None,
                false => Some(line.to_string()),
            })
            .collect::<Vec<String>>();
        buff.sort();
        Ok(buff)
    }

    async fn read_repo_packagelist(&self) -> Result<Vec<String>> {
        let cmd = Command::new("pacman")
            .args(["-Ssq"])
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let buff = output
            .split('\n')
            .filter_map(|line| match line.is_empty() {
                true => None,
                false => Some(line.to_string()),
            })
            .collect::<Vec<String>>();
        Ok(buff)
    }

    async fn get_package_list(&self) -> Result<Vec<String>> {
        if self.pacman_wrapper.is_some() {
            let mut packages = vec![];
            let (repo_packages, aur_packages) =
                join!(self.read_repo_packagelist(), self.fetch_aur_package_list());
            packages.extend(repo_packages?);
            packages.extend(aur_packages?);
            packages.sort();
            Ok(packages)
        } else {
            let mut packages = self.read_repo_packagelist().await?;
            packages.sort();
            Ok(packages)
        }
    }

    async fn get_installed_packages(&self) -> Result<Vec<String>> {
        let cmd = Command::new("pacman")
            .args(["-Qsq"])
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let mut buff = output
            .split('\n')
            .filter_map(|line| match line.is_empty() {
                true => None,
                false => Some(line.to_string()),
            })
            .collect::<Vec<String>>();
        buff.sort();
        Ok(buff)
    }
}

#[async_trait]
impl PackageManager for Pacman {
    async fn install(&self, query: Option<Vec<String>>) -> Result<()> {
        exec_command_verbose(&["sudo", "pacman", "-Syy"]).await?;
        if let Some(packages) = query {
            match &self.pacman_wrapper {
                Some(pacman_wrapper) => {
                    for package in packages.iter() {
                        pacman_wrapper.install(package).await.context(format!(
                            "failed to install package {package} using {}",
                            pacman_wrapper.name()
                        ))?;
                    }
                }
                None => {
                    for package in packages.iter() {
                        exec_command_verbose(&["sudo", "pacman", "-Syyuu", package.as_str()])
                            .await
                            .context(format!("failed to install package {package} using pacman"))?;
                    }
                }
            };
            return Ok(());
        }
        let packages = self.get_package_list().await?;
        if !packages.is_empty() {
            let selected_package = self.get_selected_package(&packages)?;
            match &self.pacman_wrapper {
                Some(pacman_wrapper) => {
                    pacman_wrapper.install(&selected_package).await?;
                }
                None => {
                    exec_command_verbose(&["sudo", "pacman", "-Syyuu", selected_package.as_str()])
                        .await?;
                }
            };
        }
        Ok(())
    }

    async fn delete(&self, query: Option<Vec<String>>) -> Result<()> {
        if let Some(packages) = query {
            for package in packages.iter() {
                exec_command_verbose(&["sudo", "pacman", "-Rsn", package])
                    .await
                    .context(format!("failed to remove package {package}"))?;
            }
            return Ok(());
        }
        let packages = self.get_installed_packages().await?;
        if !packages.is_empty() {
            let selected_package = self.get_selected_package(&packages)?;
            exec_command_verbose(&["sudo", "pacman", "-Rsn", selected_package.as_str()]).await?;
        }
        Ok(())
    }

    async fn update(&self) -> Result<()> {
        exec_command_verbose(&[
            "sudo",
            "pacman",
            "-Syyuu",
            "--noconfirm",
            "--needed",
            "--overwrite",
            "\"*\"",
        ])
        .await?;
        if let Some(pacman_wrapper) = &self.pacman_wrapper {
            pacman_wrapper.update().await?;
        }
        Ok(())
    }

    async fn info(&self, query: Option<String>) -> Result<()> {
        if let Some(package) = query {
            match &self.pacman_wrapper {
                Some(pacman_wrapper) => {
                    pacman_wrapper.info(&package).await?;
                }
                None => {
                    exec_command_verbose(&["sudo", "pacman", "-Si", package.as_str()]).await?;
                    exec_command_verbose(&["sudo", "pacman", "-Qs", package.as_str()]).await?;
                    exec_command_verbose(&["sudo", "pacman", "-Fl", package.as_str()]).await?;
                }
            };
        }
        Ok(())
    }
}
