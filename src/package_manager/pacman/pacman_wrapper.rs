use anyhow::Result;

use crate::shell::command_exists;
use crate::shell::exec_command_verbose;

#[derive(Debug)]
pub(crate) enum PacmanWrapper {
    Paru,
    Yay,
}

impl PacmanWrapper {
    /// Searches for installed pacman wrappers
    pub(crate) async fn detect_existing() -> Option<Self> {
        if command_exists("paru").await {
            return Some(PacmanWrapper::Paru);
        }
        if command_exists("yay").await {
            return Some(PacmanWrapper::Yay);
        }
        None
    }

    pub(crate) fn name(&self) -> &'static str {
        match self {
            PacmanWrapper::Paru => "paru",
            PacmanWrapper::Yay => "yay",
        }
    }

    pub(crate) async fn install(&self, package_name: &str) -> Result<()> {
        match self {
            PacmanWrapper::Paru => exec_command_verbose(&["paru", "-Syyuu", package_name]).await?,
            PacmanWrapper::Yay => exec_command_verbose(&["yay", "-Syyuu", package_name]).await?,
        };
        Ok(())
    }

    pub(crate) async fn update(&self) -> Result<()> {
        match self {
            PacmanWrapper::Paru => {
                exec_command_verbose(&[
                    "paru",
                    "-Syyuu",
                    "--noconfirm",
                    "--sudoloop",
                    "--overwrite",
                    "\"*\"",
                ])
                .await?
            }
            PacmanWrapper::Yay => {
                exec_command_verbose(&[
                    "yay",
                    "-Syyuu",
                    "--noconfirm",
                    "--sudoloop",
                    "--overwrite",
                    "\"*\"",
                ])
                .await?
            }
        };
        Ok(())
    }

    pub(crate) async fn info(&self, package_name: &str) -> Result<()> {
        match self {
            PacmanWrapper::Paru => {
                let _ = exec_command_verbose(&["paru", "-Si", package_name]).await;
                let _ = exec_command_verbose(&["paru", "-Qs", package_name]).await;
                let _ = exec_command_verbose(&["paru", "-Fl", package_name]).await;
            }
            PacmanWrapper::Yay => {
                let _ = exec_command_verbose(&["yay", "-Si", package_name]).await;
                let _ = exec_command_verbose(&["yay", "-Qs", package_name]).await;
                let _ = exec_command_verbose(&["yay", "-Fl", package_name]).await;
            }
        };
        Ok(())
    }
}
