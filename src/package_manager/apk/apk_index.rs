use std::io::Read;

use anyhow::anyhow;
use anyhow::Result;
use flate2::read::MultiGzDecoder;
use futures::future::join_all;
use tar::Archive;
use tokio::fs;

#[derive(Debug, Default, Clone)]
pub(crate) struct ApkPackage {
    pub name: String,
    pub version: String,
    pub arch: String,
    pub description: String,
    pub url: String,
    pub license: String,
    pub maintainer: String,
    pub depends: String,
    pub size: String,
    pub installed_size: String,
    pub csum: String,
    pub provides: String,
    pub install_if: String,
    pub origin: String,
    pub build_time: String,
    pub commit: String,
    pub provider_priority: String,
}

#[derive(Debug, Default)]
pub(crate) struct ApkIndex {
    packages: Vec<ApkPackage>,
}

impl ApkIndex {
    pub(crate) async fn new() -> Result<ApkIndex> {
        let mut apk_index = ApkIndex::default();
        apk_index.load_indices().await?;
        Ok(apk_index)
    }

    async fn load_indices(&mut self) -> Result<()> {
        let mut queue = vec![];
        let database_paths = ApkIndex::find_apk_index_database_paths().await?;
        for path in database_paths {
            queue.push(ApkIndex::extract_apkindex_gzip(path));
        }
        let queue = join_all(queue).await;
        for result in queue {
            self.load_str(&result?);
        }
        Ok(())
    }

    fn load_str(&mut self, data: &str) {
        let mut apk_package: Option<ApkPackage> = None;
        for line in data.split('\n') {
            if line.is_empty() {
                if let Some(elem) = apk_package.take() {
                    self.packages.push(elem);
                }
                continue;
            }
            if line.len() < 2 {
                continue;
            }
            if apk_package.is_none() {
                apk_package = Some(ApkPackage::default());
            }
            if let Some(apk_package_mut) = apk_package.as_mut() {
                match &line[0..2] {
                    "P:" => apk_package_mut.name = line[2..].to_string(),
                    "V:" => apk_package_mut.version = line[2..].to_string(),
                    "T:" => apk_package_mut.description = line[2..].to_string(),
                    "U:" => apk_package_mut.url = line[2..].to_string(),
                    "L:" => apk_package_mut.license = line[2..].to_string(),
                    "A:" => apk_package_mut.arch = line[2..].to_string(),
                    "D:" => apk_package_mut.depends = line[2..].to_string(),
                    "C:" => apk_package_mut.csum = line[2..].to_string(),
                    "S:" => apk_package_mut.size = line[2..].to_string(),
                    "I:" => apk_package_mut.installed_size = line[2..].to_string(),
                    "p:" => apk_package_mut.provides = line[2..].to_string(),
                    "i:" => apk_package_mut.install_if = line[2..].to_string(),
                    "o:" => apk_package_mut.origin = line[2..].to_string(),
                    "m:" => apk_package_mut.maintainer = line[2..].to_string(),
                    "t:" => apk_package_mut.build_time = line[2..].to_string(),
                    "c:" => apk_package_mut.commit = line[2..].to_string(),
                    "k:" => apk_package_mut.provider_priority = line[2..].to_string(),
                    field => println!("Unknown Field: {field}"),
                };
            }
        }
    }

    async fn extract_apkindex_gzip(path: String) -> Result<String> {
        let tar_gz = std::fs::File::open(&path)?;
        let tar = MultiGzDecoder::new(tar_gz);
        let mut archive = Archive::new(tar);
        let entries = archive.entries()?;
        for mut entry in entries.flatten() {
            if let Some(path) = entry.header().path()?.to_str() {
                if path == "APKINDEX" {
                    let mut content = String::new();
                    entry.read_to_string(&mut content)?;
                    return Ok(content);
                }
            }
        }
        Err(anyhow!("Failed to extract APKINDEX data from: {}", &path))
    }

    async fn find_apk_index_database_paths() -> Result<Vec<String>> {
        let mut buff = vec![];
        let mut apk_index_archives = fs::read_dir("/var/cache/apk/").await?;
        while let Ok(Some(path)) = apk_index_archives.next_entry().await {
            let path_string = path.path().display().to_string();
            if path_string.starts_with("/var/cache/apk/APKINDEX") && path_string.ends_with("tar.gz")
            {
                buff.push(path_string);
            }
        }
        buff.shrink_to_fit();
        Ok(buff)
    }

    pub(crate) async fn get_packages_where_name_contains(&self, query: &str) -> Vec<ApkPackage> {
        self.packages
            .clone()
            .into_iter()
            .filter(|p| p.name.contains(query))
            .collect::<Vec<ApkPackage>>()
    }

    pub(crate) fn get_all_package_names(&self) -> Vec<&str> {
        self.packages.iter().map(|p| p.name.as_str()).collect()
    }
}
