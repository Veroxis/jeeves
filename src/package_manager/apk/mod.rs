mod apk_index;

use std::process::Stdio;

use anyhow::Context;
use anyhow::Result;
use apk_index::ApkIndex;
use async_trait::async_trait;
use tokio::fs;
use tokio::process::Command;

use super::PackageManager;
use crate::shell::exec_command_verbose;

#[derive(Debug, Default)]
pub(crate) struct Apk {}

impl Apk {
    pub(crate) fn new() -> Self {
        Self {}
    }

    async fn get_package_list(&self) -> Result<Vec<String>> {
        let apk_index = ApkIndex::new().await?;
        let mut packages = apk_index
            .get_all_package_names()
            .iter()
            .map(|p| p.to_string())
            .collect::<Vec<String>>();
        packages.sort();
        Ok(packages)
    }

    async fn get_installed_packages(&self) -> Result<Vec<String>> {
        let cmd = Command::new("apk")
            .args(["info"])
            .stderr(Stdio::inherit())
            .stdin(Stdio::piped())
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let mut buff = output
            .split('\n')
            .map(|line| line.to_string())
            .filter(|line| !line.is_empty())
            .collect::<Vec<String>>();
        buff.sort();
        Ok(buff)
    }
}

#[async_trait]
impl PackageManager for Apk {
    async fn install(&self, query: Option<Vec<String>>) -> Result<()> {
        exec_command_verbose(&["sudo", "apk", "update"]).await?;
        if let Some(packages) = query {
            for package in packages.iter() {
                exec_command_verbose(&["sudo", "apk", "add", package])
                    .await
                    .context(format!("failed to install package {package}"))?;
            }
            return Ok(());
        }
        let packages = self.get_package_list().await?;
        if !packages.is_empty() {
            if fs::read_dir("/etc/terminfo").await.is_err() {
                exec_command_verbose(&["sudo", "apk", "add", "ncurses-terminfo-base"]).await?;
            }
            let selected_package = self.get_selected_package(&packages)?;
            exec_command_verbose(&["sudo", "apk", "add", selected_package.as_str()]).await?;
        }
        Ok(())
    }

    async fn delete(&self, query: Option<Vec<String>>) -> Result<()> {
        if let Some(packages) = query {
            for package in packages.iter() {
                exec_command_verbose(&["sudo", "apk", "del", package])
                    .await
                    .context(format!("failed to remove package {package}"))?;
            }
            return Ok(());
        }
        let packages = self.get_installed_packages().await?;
        if !packages.is_empty() {
            if fs::read_dir("/etc/terminfo").await.is_err() {
                exec_command_verbose(&["sudo", "apk", "add", "ncurses-terminfo-base"]).await?;
            }
            let selected_package = self.get_selected_package(&packages)?;
            exec_command_verbose(&["sudo", "apk", "del", selected_package.as_str()]).await?;
        }
        Ok(())
    }

    async fn update(&self) -> Result<()> {
        exec_command_verbose(&["sudo", "apk", "update"]).await?;
        exec_command_verbose(&["sudo", "apk", "upgrade"]).await?;
        Ok(())
    }

    async fn info(&self, query: Option<String>) -> Result<()> {
        if let Some(package_query) = query {
            let apk_index = ApkIndex::new().await?;
            let packages = apk_index
                .get_packages_where_name_contains(&package_query)
                .await;
            for package in packages.iter() {
                if package.name == package_query {
                    println!("{package:#?}");
                }
            }
        }
        Ok(())
    }
}
