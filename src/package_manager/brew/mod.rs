use std::process::Stdio;

use anyhow::Context;
use anyhow::Result;
use async_trait::async_trait;
use tokio::process::Command;

use super::PackageManager;
use crate::shell::exec_command_verbose;
use crate::shell::get_current_executable_path;

#[derive(Debug, Default)]
pub(crate) struct Brew {}

impl Brew {
    pub(crate) fn new() -> Self {
        Self {}
    }

    async fn get_package_list(&self) -> Result<Vec<String>> {
        let cmd = Command::new("brew")
            .args(["search", ""])
            .stderr(Stdio::inherit())
            .stdin(Stdio::piped())
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let mut buff = output
            .split('\n')
            .filter_map(|line| match line.is_empty() {
                true => None,
                false => Some(line.to_string().replace('\t', " ")),
            })
            .collect::<Vec<String>>();
        buff.sort();
        Ok(buff)
    }

    async fn get_installed_packages(&self) -> Result<Vec<String>> {
        let cmd = Command::new("brew")
            .args(["list", "--installed-on-request"])
            .stderr(Stdio::inherit())
            .stdin(Stdio::piped())
            .stdin(Stdio::inherit())
            .output()
            .await?;
        let output = std::str::from_utf8(&cmd.stdout)?;
        let mut buff = output
            .split('\n')
            .filter_map(|line| match line.is_empty() {
                true => None,
                false => Some(line.replace('\t', " ")),
            })
            .collect::<Vec<String>>();
        buff.sort();
        Ok(buff)
    }
}

#[async_trait]
impl PackageManager for Brew {
    fn get_preview_command(&self) -> Result<String> {
        Ok(format!(
            "{jeeves} info --brew {{1}}",
            jeeves = get_current_executable_path()?
        ))
    }

    async fn install(&self, query: Option<Vec<String>>) -> Result<()> {
        exec_command_verbose(&["brew", "update"]).await?;
        if let Some(packages) = query {
            for package in packages.iter() {
                exec_command_verbose(&["brew", "install", package])
                    .await
                    .context(format!("failed to install package {package}"))?;
            }
            return Ok(());
        }
        let packages = self.get_package_list().await?;
        if !packages.is_empty() {
            let package = self.get_selected_package(&packages)?;
            exec_command_verbose(&["brew", "install", package.as_str()]).await?;
        }
        Ok(())
    }

    async fn delete(&self, query: Option<Vec<String>>) -> Result<()> {
        if let Some(packages) = query {
            for package in packages.iter() {
                exec_command_verbose(&["brew", "uninstall", package])
                    .await
                    .context(format!("failed to remove package {package}"))?;
            }
            return Ok(());
        }
        let packages = self.get_installed_packages().await?;
        if !packages.is_empty() {
            let package = self.get_selected_package(&packages)?;
            exec_command_verbose(&["brew", "uninstall", package.as_str()]).await?;
        }
        Ok(())
    }

    async fn update(&self) -> Result<()> {
        exec_command_verbose(&["brew", "update"]).await?;
        exec_command_verbose(&["brew", "upgrade"]).await?;
        Ok(())
    }

    async fn info(&self, query: Option<String>) -> Result<()> {
        if let Some(package) = query {
            let _ = exec_command_verbose(&["brew", "info", package.as_str()]).await;
        }
        Ok(())
    }
}
