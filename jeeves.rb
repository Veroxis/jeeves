class Jeeves < Formula
  desc "Jeeves is a command line utility which acts as a layer above your OS package manager. It provides an interactive package browser within the CLI."
  homepage "https://gitlab.com/Veroxis/jeeves"
  url "https://gitlab.com/Veroxis/jeeves/-/archive/v2.0.0/jeeves-v2.0.0.tar"
  version "2.0.0"

  def install
    if OS.linux?
      system "sh", "-c", <<-EOS
        curl --proto "=https" "--tlsv1.2" -sSf "https://sh.rustup.rs" | sh -y
      EOS
      # system "rustup", "default", "stable"
      # if Hardware::CPU.intel?
      #   system "rustup", "target", "add", "x86_64-unknown-linux-musl"
      #   system "cargo", "build", "--release", "--target", "x86_64-unknown-linux-musl"
      #   bin.install "target/x86_64-unknown-linux-musl/release/jeeves" => "jeeves"
      # elsif Hardware::CPU.arm?
      #   system "rustup", "target", "add", "aarch64-unknown-linux-musl"
      #   system "cargo", "build", "--release", "--target", "aarch64-unknown-linux-musl"
      #   bin.install "target/aarch64-unknown-linux-musl/release/jeeves" => "jeeves"
      # end
    end
  end

end
